//
//  FinalSplitScreenViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 01/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FinalSplitScreenViewController.h"
#import "Receipt.h"
#import "ReceiptOrderCell.h"
#import "Order.h"
#import "CurrentUser.h"
#import "AFNetworking.h"
#import "ActivityIndicator.h"

static NSString *ReceiptOrderCellIdentifier = @"ReceiptOrderCellIdentifier";

@interface FinalSplitScreenViewController ()

@end

@implementation FinalSplitScreenViewController

- (id)initWithConfirmedOrder:(ConfirmedOrder *)aConfirmedOrder
{
    self = [super initWithNibName:@"FinalSplitScreenViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        confirmedOrder = aConfirmedOrder;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.orderTableView.rowHeight = 22;
    [self.orderTableView registerNib:[UINib nibWithNibName:@"ReceiptOrderCell" bundle:nil] forCellReuseIdentifier:ReceiptOrderCellIdentifier];
    

    
    [self configureNavBarWithLeftButton:kHomeButton titleButton:kButtonTypeNone andRightButton:kButtonTypeNone];
    
    [self fillData];
    [self uploadOrderData];
}

#pragma mark - Own Methods
- (void)fillData
{
    self.addressLbl.text = confirmedOrder.deliveryAddress;
    
    self.subTotalLbl.text = [NSString stringWithFormat:@"$ %0.2f", [confirmedOrder.receipt getSubTotalAmount]];
    self.taxLbl.text = [NSString stringWithFormat:@"$ %0.2f", [confirmedOrder.receipt getTaxAmount]];
    self.totalItemLbl.text = [NSString stringWithFormat:@"%2lu", confirmedOrder.receipt.orders.count];
    self.totalAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", [confirmedOrder.receipt getTotalAmount]];
    
    SplitData *paidData = [confirmedOrder.splitUsers firstObject];
    
    self.paidAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", paidData.amount];
    
    float remainingAmount = [confirmedOrder.receipt getTotalAmount] - paidData.amount;
    self.remainingAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", remainingAmount];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE";
    
    self.dayLbl.text = [dateFormatter stringFromDate:confirmedOrder.deliveryTime];
    
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    self.timeLbl.text = [dateFormatter stringFromDate:confirmedOrder.deliveryTime];
    
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    
    self.dateLbl.text = [dateFormatter stringFromDate:confirmedOrder.deliveryTime];
}

- (void)uploadOrderData
{
    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"object": confirmedOrder};
    
    [manager POST:ORDER_UPLOAD_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Got response: %@", responseObject);
        NSLog(@"Uploaded confirmed order %@", confirmedOrder);
        
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Got error: %@", error);
        NSLog(@"Failed uploading completed order %@", confirmedOrder);
        
        [Utility showAlert:@"Cannot upload your order. Please try again"];
        
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    }];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    UINavigationController *presentingVC = (UINavigationController *)self.navigationController.presentingViewController;
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [presentingVC popToRootViewControllerAnimated:YES];
    }];
    
//    [[CurrentUser getReceipts] addObject:[CurrentUser getCurrentReceipt]];
//    [CurrentUser getCurrentUser].currentReceipt = [[Receipt alloc] init];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (IBAction)homeBtnPressed:(id)sender {
    UINavigationController *presentingVC = (UINavigationController *)self.navigationController.presentingViewController;
    [self.navigationController dismissViewControllerAnimated:NO completion:^{
        [presentingVC popToRootViewControllerAnimated:YES];
    }];
    
    [[CurrentUser getReceipts] addObject:[CurrentUser getCurrentReceipt]];
    [CurrentUser getCurrentUser].currentReceipt = [[Receipt alloc] init];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return confirmedOrder.receipt.orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ReceiptOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:ReceiptOrderCellIdentifier];
    if (cell == nil) {
        cell = [[ReceiptOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ReceiptOrderCellIdentifier];
    }
    
    Order *order = [confirmedOrder.receipt.orders objectAtIndex:indexPath.row];
    
    cell.backgroundColor = [UIColor clearColor];
    cell.receiptOrderName.font = [UIFont systemFontOfSize:10];
    cell.receiptOrderQuantity.font = [UIFont systemFontOfSize:10];
    cell.receiptOrderAmount.font = [UIFont systemFontOfSize:12];
    
    cell.receiptOrderName.text = order.menuName;
    cell.receiptOrderQuantity.text = [NSString stringWithFormat:@"%d x", order.quantity];
    cell.receiptOrderAmount.text = [NSString stringWithFormat:@"$ %0.2f", [order getOrderTotalPrice]];
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
@end
