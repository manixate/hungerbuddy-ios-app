//
//  DealsDetailViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 12/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Deal;

@interface DealsDetailViewController : UIViewController

- (id)initWithDeal:(Deal *)aDeal;

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *restaurantLbl;
@property (weak, nonatomic) IBOutlet UILabel *restaurantAddressLbl;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeLbl;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;

@property (weak, nonatomic) IBOutlet UIView *daysView;
@property (weak, nonatomic) IBOutlet UIView *hoursView;
@property (weak, nonatomic) IBOutlet UIView *minutesView;

- (IBAction)shareBtnPressed:(id)sender;
@end
