//
//  SplitUserViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 22/02/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "SplitUserViewController.h"
#import "CurrentUser.h"
#import "SplitData.h"
#import "SplitUserCell.h"
#import "KNMultiItemSelector.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"
#import <ActionSheetStringPicker.h>

static NSString *SplitUserCellIdentifier = @"SplitUserCellIdentifier";

@interface SplitUserViewController () <SplitUserCellDelegate, KNMultiItemSelectorDelegate>
{
    NSInteger minutes;
    
    double totalAmount;
    BOOL splitEqually;
    NSMutableArray *tempArr;
    
    ActionSheetStringPicker *timePickerView;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIButton *splitEqualBtn;
@property (weak, nonatomic) IBOutlet UIButton *splitCustomBtn;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITableView *splitUserTableView;
@property (weak, nonatomic) IBOutlet UILabel *remainingAmountLbl;
@property (weak, nonatomic) IBOutlet UIButton *setTimeLimitBtn;

- (IBAction)splitEquallyBtnPressed:(id)sender;
- (IBAction)splitCustomBtnPressed:(id)sender;
- (IBAction)addUserBtnPressed:(id)sender;
- (IBAction)friendListBtnPressed:(id)sender;
- (IBAction)setTimeLimitBtnPressed:(id)sender;

@end

@implementation SplitUserViewController

- (id)initWithSplitArray:(NSArray *)splitArray totalAmount:(double)aTotalAmount andMinutes:(NSInteger)aMinutes
{
    self = [super initWithNibName:@"SplitUserViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        tempArr = [NSMutableArray arrayWithArray:splitArray];
        
        splitEqually = true;
        
        totalAmount = aTotalAmount;
        
        minutes = aMinutes;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.splitUserTableView.rowHeight = 44;
    
    [self.splitUserTableView registerNib:[UINib nibWithNibName:@"SplitUserCell" bundle:[NSBundle mainBundle]]
                   forCellReuseIdentifier:SplitUserCellIdentifier];
    
    [self.splitEqualBtn setImage:[UIImage imageNamed:@"ring.PNG"] forState:UIControlStateNormal];
    [self.splitEqualBtn setImage:[UIImage imageNamed:@"tick.PNG"] forState:UIControlStateSelected];
    
    [self.splitCustomBtn setImage:[UIImage imageNamed:@"ring.PNG"] forState:UIControlStateNormal];
    [self.splitCustomBtn setImage:[UIImage imageNamed:@"tick.PNG"] forState:UIControlStateSelected];
    
    self.splitEqualBtn.selected = true;

    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kConfirmButton];
    
    [self reloadData];
    
    [self refreshSpinners];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.delegate splitUsersListConfirmed:@[tempArr.firstObject] andMinutes:minutes]; // Just the user
}

- (IBAction)rightBtnPressed:(id)sender {
    [self.delegate splitUsersListConfirmed:tempArr andMinutes:minutes];
}

- (IBAction)splitEquallyBtnPressed:(id)sender {
    self.splitCustomBtn.selected = false;
    self.splitEqualBtn.selected = true;
    
    splitEqually = true;
    
    [self reloadData];
}

- (IBAction)splitCustomBtnPressed:(id)sender {
    self.splitCustomBtn.selected = true;
    self.splitEqualBtn.selected = false;
    
    splitEqually = false;
    
    [self reloadData];
}

- (IBAction)addUserBtnPressed:(id)sender {
    NSString *email = self.emailTextField.text;
    
    if (![email isEqualToString:@""]) {
        SplitData *data = [[SplitData alloc] init];
        data.email = email;
        data.amount = 0.0f;
        
        [tempArr addObject:data];
        
        [self reloadData];
        
        self.emailTextField.text = @"";
    }
}

- (IBAction)friendListBtnPressed:(id)sender {
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:[NSString stringWithFormat:@"%@%@", URL_BASE, GET_ALL_USERS]
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)[operation.response statusCode]);
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             [Utility showAlert:@"Your friend list is empty. Please add some friends."];
             
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSMutableArray *emails = [NSMutableArray array];
         
         for (NSDictionary *userData in json) {
             NSString *email = [userData objectForKey:@"email"];
             
             if (!email || [email isEqualToString:@""])
                 continue;
             
             KNSelectorItem *item = [[KNSelectorItem alloc] initWithDisplayValue:email];
             
             [emails addObject:item];
         }
         
         KNMultiItemSelector *selector = [[KNMultiItemSelector alloc] initWithItems:emails delegate:self];
         
         [self.navigationController pushViewController:selector animated:YES];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld",(long)operation.response.statusCode);
         [Utility showAlert:@"Error while fetching friend list data."];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

- (IBAction)setTimeLimitBtnPressed:(id)sender {
    NSMutableArray *minutesArray = [NSMutableArray array];
    for (int i = 0; i < 60; i++)
         [minutesArray addObject:@(i)];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Time limit (in minutes)" rows:minutesArray initialSelection:minutes doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        minutes = [selectedValue intValue];
        [self refreshSpinners];
        
    } cancelBlock:nil origin:sender];
}

#pragma mark - Own Methods
- (void)reloadData {
    float totalAmountDivided = 0;
    
    if (!splitEqually)
    {
        for (SplitData *data in tempArr) {
            totalAmountDivided += data.amount;
        }
    }
    else
    {
        float equalAmount = totalAmount / tempArr.count;
        for (SplitData *data in tempArr) {
            data.amount = equalAmount;
            totalAmountDivided += data.amount;
        }
    }
    
    self.remainingAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", totalAmount - totalAmountDivided];
    
    [self.splitUserTableView reloadData];
}

-(void)refreshSpinners {
    [self.setTimeLimitBtn setTitle:[NSString stringWithFormat:@"Time limit: %ld min", (long)minutes] forState:UIControlStateNormal];
}

#pragma mark - SplitUserCellDelegate Methods
- (void)amountChanged:(float)amount forIndex:(int)index {
    SplitData *splitData = [tempArr objectAtIndex:index];
    splitData.amount = amount;
    
    [self reloadData];
}

#pragma mark - KNMultiItemSelectorDelegate
- (void)selectorDidCancelSelection {
    [self reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)selector:(KNMultiItemSelector *)selector didFinishSelectionWithItems:(NSArray *)selectedItems {
    for (KNSelectorItem *item in selectedItems)
    {
        SplitData *splitData = [[SplitData alloc] init];
        splitData.email = item.selectValue;
        splitData.amount = 0.0f;
        
        [tempArr addObject:splitData];
    }
    
    [self reloadData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return tempArr.count;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0)
        return false;
    
    return true;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UITableViewCellEditingStyleDelete == editingStyle)
    {
        [tempArr removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        [self reloadData];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SplitData *splitData = [tempArr objectAtIndex:indexPath.row];
    
    NSString *cellIdentifier = SplitUserCellIdentifier;
    
    SplitUserCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[SplitUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [cell setupKeyboard];
    
    cell.index = indexPath.row;
    cell.delegate = self;
    
    cell.nameLbl.text = splitData.name;
    cell.indexLbl.text = [NSString stringWithFormat:@"%ld.", indexPath.row];
    cell.emailAddressLbl.text = splitData.email;
    cell.amountTextField.text = [NSString stringWithFormat:@"$ %0.2f", splitData.amount];
    
    cell.amountTextField.enabled = !splitEqually;
    
    [cell refreshView];
    
    return cell;
}

@end
