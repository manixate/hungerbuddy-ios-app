//
//  FoodCategory.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FoodCategory.h"

@implementation FoodCategory

- (id)initWithFoodCategory:(FoodCategory *)category {
    if (self = [super init]) {
        self.ID = category.ID;
        self.categoryName = category.categoryName;
        self.categoryDescription = category.categoryDescription;
        self.categoryStatus = category.categoryStatus;
        self.dateAdded = category.dateAdded;
        
        self.foodList = [NSArray arrayWithArray:category.foodList];
    }
    
    return self;
}


@end
