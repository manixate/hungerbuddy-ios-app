//
//  FoodCategory.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Category.h"

@interface FoodCategory : Category

@property (nonatomic, strong) NSArray *foodList;

- (id)initWithFoodCategory:(FoodCategory *)category;

@end
