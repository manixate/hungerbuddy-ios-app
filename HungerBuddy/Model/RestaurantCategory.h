//
//  RestaurantCategory.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Category.h"

@interface RestaurantCategory : Category

@property (nonatomic, strong) NSArray *restaurant;

- (id)initWithRestaurantCategory:(RestaurantCategory *) category;

@end
