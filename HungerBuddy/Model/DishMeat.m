//
//  DishMeat.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "DishMeat.h"

@implementation DishMeat
@synthesize meat;

- (id)init {
    if (self = [super init]) {
        meat = @"";
    }
    
    return self;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"meat": @"meat",
             };
}

@end
