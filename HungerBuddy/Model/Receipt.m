//
//  Receipt.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/15/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Receipt.h"
#import "Order.h"
#import "FoodItem.h"

@implementation Receipt

- (id)init
{
    if (self = [super init])
    {
        self.orders = [[NSMutableArray alloc] init];
        self.tip = 0;
    }
    
    return self;
}

- (BOOL)containsOrder:(Order *)order {
    for (Order *o in self.orders) {
        if (order.menuID == o.menuID && order.menuSizeID == o.menuSizeID &&
            order.menuMeatID == o.menuMeatID && [order.spicy isEqualToString:o.spicy])
        {
            return true;
        }
    }
    
    return false;
}

- (Order *)getOrderFromFoodItem:(FoodItem *)item {
    Order *order = [Order orderFromFoodItem:item];
    
    for (Order *o in self.orders) {
        if (order.menuID == o.menuID && order.menuSizeID == o.menuSizeID &&
            order.menuMeatID == o.menuMeatID && [order.spicy.lowercaseString isEqualToString:o.spicy.lowercaseString])
        {
            return o;
        }
    }
    
    return nil;
}

- (void)updateOrderListWithFoodItem:(FoodItem *)item isIncrement:(BOOL)isIncrement {
    Order *order = [self getOrderFromFoodItem:item];
    
    if (order == nil && isIncrement) {
        order = [Order orderFromFoodItem:item];
        [self.orders addObject:order];
    }
    
    if (order != nil) {
        if (isIncrement)
            order.quantity += 1;
        else
            order.quantity -= 1;
        
        if (order.quantity < 1)
            [self.orders removeObject:order];
    }
}

- (void)updateOrderAtIndex:(int)index isIncrement:(BOOL)isIncrement {
    Order *order = [self.orders objectAtIndex:index];
    
    if (order != nil) {
        if (isIncrement)
            order.quantity += 1;
        else
            order.quantity -= 1;
    }
}

- (float)getTotalAmount {
    float amount = 0.0f;
    amount = [self getSubTotalAmount] + [self getTaxAmount] + self.tip;
    
    return amount;
}

- (float)getSubTotalAmount {
    float amount = 0.0f;
    for (Order *order in self.orders) {
        amount += [order getOrderTotalPrice];
    }
    
    return amount;
}

- (float)getTaxAmount {
    float amount = [self getSubTotalAmount];
    amount = amount * 13.0f / 100.0f;
    
    return amount;
}

@end
