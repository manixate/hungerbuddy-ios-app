//
//  Receipt.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/15/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Order, FoodItem, Restaurant;

@interface Receipt : NSObject

@property (nonatomic, strong) Restaurant *restaurant;
@property (nonatomic, strong) NSMutableArray *orders;
@property (nonatomic) float tip;

- (float) getSubTotalAmount;
- (float) getTaxAmount;
- (float) getTotalAmount;

- (BOOL) containsOrder:(Order *)order;
- (Order *) getOrderFromFoodItem:(FoodItem *)item;
- (void) updateOrderListWithFoodItem:(FoodItem *)item isIncrement:(BOOL)isIncrement;
- (void) updateOrderAtIndex:(int)index isIncrement:(BOOL) isIncrement;

@end
