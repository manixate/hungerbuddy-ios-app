//
//  DishInfo.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 8/30/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "DishInfo.h"

@implementation DishInfo
@synthesize ID, restaurantMenuID, dishInfoDescription, price, isDefault, sequenceNo;

- (id)init {
    if (self = [super init]) {
        ID = -1;
        restaurantMenuID = -1;
        dishInfoDescription = @"";
        price = 0;
        isDefault = false;
        sequenceNo = -1;
    }
    
    return self;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"restaurantMenuID": @"restaurant_menu_id",
             @"price": @"price",
             @"dishInfoDescription": @"description",
             @"isDefault": @"is_default",
             @"sequenceNo": @"seq_no",
             };
}

+ (NSValueTransformer *)isDefaultJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *val) {
        if ([@"1" isEqualToString:val])
            return [NSNumber numberWithBool:true];
        else
            return [NSNumber numberWithBool:false];
    }];
}


@end
