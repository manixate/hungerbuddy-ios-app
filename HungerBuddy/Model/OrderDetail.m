//
//  OrderDetail.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 6/10/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "OrderDetail.h"

@implementation OrderDetail

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"amount": @"bill_amount",
             @"isVerified": @"cc_verified",
             @"isProcessed": @"cc_processed",
             @"orderToken": @"order_monitor_token"
             };
}

@end
