//
//  ConfirmedOrder.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 02/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SplitData.h"

@class Receipt;

@interface ConfirmedOrder : NSObject

@property (nonatomic, strong) NSString *deliveryAddress;
@property (nonatomic, strong) NSDate *deliveryTime;
@property (nonatomic, strong) Receipt *receipt;
@property (nonatomic, strong) NSArray *splitUsers;

@end