//
//  User.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize userID, userName, userAddress = _userAddress, userEmail, userCity, userType, userImageURL, userWallURL;

- (id)init
{
    if (self = [super init])
    {
        userID = -1;
        userName = @"";
        userEmail = @"";
        _userAddress = @"";
        userCity = @"";
        userImageURL = nil;
        userWallURL = nil;
        userType = -1;
    }
    return self;
}

- (NSString *)userAddress
{
    if (!_userAddress || [_userAddress isEqualToString:@""])
        return @"No address specified";
    return _userAddress;
}

#pragma mark - NSCoding Methods
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        userID = [aDecoder decodeIntForKey:@"userID"];
        userName = [aDecoder decodeObjectForKey:@"userName"];
        userEmail = [aDecoder decodeObjectForKey:@"userEmail"];
        _userAddress = [aDecoder decodeObjectForKey:@"_userAddress"];
        userCity = [aDecoder decodeObjectForKey:@"userCity"];
        userImageURL = [aDecoder decodeObjectForKey:@"userImageURL"];
        userWallURL = [aDecoder decodeObjectForKey:@"userWallURL"];
        userType = [aDecoder decodeIntForKey:@"userType"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeInt:userID forKey:@"userID"];
    [aCoder encodeObject:userName forKey:@"userName"];
    [aCoder encodeObject:userEmail forKey:@"userEmail"];
    [aCoder encodeObject:_userAddress forKey:@"_userAddress"];
    [aCoder encodeObject:userCity forKey:@"userCity"];
    [aCoder encodeObject:userImageURL forKey:@"userImageURL"];
    [aCoder encodeObject:userWallURL forKey:@"userWallURL"];
    [aCoder encodeInteger:userType forKey:@"userType"];
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"userID": @"id",
             @"userName": @"full_name",
             @"userEmail": @"email",
             @"userAddress": @"address",
             @"userCity": @"city",
             @"userImageURL": @"image",
             @"userWallURL": @"url",
             @"userType": @"type",
             };
}

+ (NSValueTransformer *)userImageURLJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)userWallURLJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *urlString) {
        if (!urlString || [urlString isEqualToString:@""])
            return nil;
        
        return [NSURL URLWithString:urlString relativeToURL:[NSURL URLWithString:URL_BASE]];
    }];
}

@end
