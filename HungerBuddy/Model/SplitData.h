//
//  SplitData.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 11/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SplitData : NSObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *email;
@property (nonatomic) float amount;

@end
