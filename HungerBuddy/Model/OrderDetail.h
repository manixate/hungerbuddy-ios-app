//
//  OrderDetail.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 6/10/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderDetail : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *email;
@property (nonatomic) bool isVerified;
@property (nonatomic) bool isProcessed;
@property (nonatomic) double amount;
@property (nonatomic, strong) NSString *orderToken;

@end
