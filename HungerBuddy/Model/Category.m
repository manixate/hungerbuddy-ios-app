//
//  RestaurantCategory.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Category.h"

@implementation Category

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"categoryName": @"category_name",
             @"categoryDescription": @"description",
             @"categoryStatus": @"category_status"
             };
}

+ (NSValueTransformer *)categoryStatusJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *categoryStatus) {
        return @([@"1" isEqualToString:categoryStatus]);
    }];
}

@end
