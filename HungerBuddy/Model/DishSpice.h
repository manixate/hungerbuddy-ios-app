//
//  DishSpice.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishInfo.h"

@interface DishSpice : DishInfo

@property (nonatomic, strong) NSString *spice;

@end
