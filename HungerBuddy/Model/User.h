//
//  User.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : MTLModel <MTLJSONSerializing>

@property (nonatomic) int userID;

@property (nonatomic, strong, getter = getUserName) NSString *userName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userAddress;
@property (nonatomic, strong) NSString *userCity;
@property (nonatomic) int userType;
@property (nonatomic, strong) NSURL *userImageURL;
@property (nonatomic, strong) NSURL *userWallURL;

@end