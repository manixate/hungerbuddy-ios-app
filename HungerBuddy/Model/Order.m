//
//  Order.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/9/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Order.h"
#import "FoodItem.h"

@implementation Order

+ (id)orderFromFoodItem:(FoodItem *)item {
    Order *order = [[Order alloc] init];
    
    order.menuID = item.ID;
    order.menuName = item.menuName;
    
    DishSize *size = [item currentSize];
    order.menuSizeID = size.ID;
    order.menuSize = size.size;
    order.sizePrice = size.price;
    
    DishMeat *meat = [item currentMeat];
    order.menuMeatID = meat.ID;
    order.menuMeat = meat.meat;
    order.meatPrice = meat.price;
    
    DishSpice *spice = [item currentSpice];
    order.spicy = spice.spice;
    
    order.price = item.price;
    
    order.quantity = 0;
    order.totalPrice = 0;
    
    order.note = item.note;
    
    return order;
}

- (float)getOrderTotalPrice {
    self.totalPrice = (self.sizePrice + self.meatPrice + self.price) * self.quantity;
    
    return self.totalPrice;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"orderID": @"order_id",
             @"menuID": @"menu_id",
             @"menuName": @"menu_name",
             @"menuSizeID": @"menu_size_id",
             @"menuSize": @"menu_size",
             @"menuMeatID": @"menu_meat_id",
             @"menuMeat": @"menu_meat",
             @"sizePrice": @"size_price",
             @"meatPrice": @"meat_price",
             @"quantity": @"qty",
             @"spicy": @"spicy",
             @"totalPrice": @"total_price"
             };
}

@end
