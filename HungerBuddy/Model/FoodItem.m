//
//  FoodItem.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FoodItem.h"

@implementation FoodItem

- (id)init {
    if (self = [super init]) {
        self.dishSizes = [[NSArray alloc] init];
        self.dishMeats = [[NSArray alloc] init];
        self.dishSpices = [[NSArray alloc] init];
        
        self.currentDishSizeIdx = 0;
        self.currentDishMeatIdx = 0;
        self.currentDishSpiceIdx = 0;
        
        self.note = @"";
    }
    
    return self;
}

- (DishSize *)currentSize {
    if (self.dishSizes.count == 0)
        return [[DishSize alloc] init];
    
    return [self.dishSizes objectAtIndex:self.currentDishSizeIdx];
}


- (DishMeat *)currentMeat {
    if (self.dishMeats.count == 0)
        return [[DishMeat alloc] init];
    
    return [self.dishMeats objectAtIndex:self.currentDishMeatIdx];
}


- (DishSpice *)currentSpice {
    if (self.dishSpices.count == 0)
        return [[DishSpice alloc] init];
    
    return [self.dishSpices objectAtIndex:self.currentDishSpiceIdx];
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"restaurantID": @"restaurant_id",
             @"menuTypeID": @"menu_type_id",
             @"menuCategoryID": @"menu_category_id",
             @"menuName": @"menu_name",
             @"foodItemDescription": @"description",
             @"price": @"price",
             @"ratingOk": @"rating_ok",
             @"ratingLiked": @"rating_liked",
             @"ratingLoved": @"rating_loved",
             @"ratingAll": @"rating_all",
             @"spiceOption": @"spice_option",
             @"chefsSpecial": @"chefs_special",
             @"vegetarian": @"vegetarian",
             @"sizes": @"sizes",
             @"dishSizes": @"dish_sizes",
             @"dishMeats": @"dish_meats",
             @"dishSpices": @"dish_spices"
             };
}

+ (NSValueTransformer *)dishSizesJSONTransformer
{
    return [MTLValueTransformer mtl_JSONArrayTransformerWithModelClass:[DishSize class]];
}

+ (NSValueTransformer *)dishMeatsJSONTransformer
{
    return [MTLValueTransformer mtl_JSONArrayTransformerWithModelClass:[DishMeat class]];
}

+ (NSValueTransformer *)dishSpicesJSONTransformer
{
    return [MTLValueTransformer mtl_JSONArrayTransformerWithModelClass:[DishSpice class]];
}

@end
