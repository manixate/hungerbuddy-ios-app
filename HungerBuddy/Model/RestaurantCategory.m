//
//  RestaurantCategory.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "RestaurantCategory.h"

@implementation RestaurantCategory

- (id)initWithRestaurantCategory:(RestaurantCategory *) category {
    if (self = [super init]) {
        self.ID = category.ID;
        self.categoryName = category.categoryName;
        self.categoryDescription = category.categoryDescription;
        self.categoryStatus = category.categoryStatus;
        self.dateAdded = category.dateAdded;
        
        self.restaurant = [NSArray arrayWithArray:category.restaurant];
    }
    
    return self;
}



@end
