//
//  DishSize.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "DishSize.h"

@implementation DishSize
@synthesize size;

- (id)init {
    if (self = [super init]) {
        size = @"";
    }
    
    return self;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"size": @"size",
             };
}

@end
