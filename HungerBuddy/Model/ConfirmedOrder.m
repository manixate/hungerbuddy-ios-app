//
//  ConfirmedOrder.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 02/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ConfirmedOrder.h"
#import "Receipt.h"

@implementation ConfirmedOrder

- (NSString *)deliveryAddress
{
    if (_deliveryAddress == nil || [_deliveryAddress isEqualToString:@""])
        return @"No Delivery Address Specified";
    
    return _deliveryAddress;
}

@end
