//
//  RestaurantCategory.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Category : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *categoryDescription;
@property (nonatomic) BOOL categoryStatus;
@property (nonatomic, strong) NSDate *dateAdded;

@end
