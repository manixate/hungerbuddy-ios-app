//
//  Restaurant.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/9/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Restaurant.h"

@implementation Restaurant

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"userID": @"user_id",
             @"categoryID": @"category_id",
             @"categoryName": @"category_name",
             @"restaurantName": @"name",
             @"address": @"address",
             @"address2": @"address2",
             @"city": @"city",
             @"postalCode": @"postal_code",
             @"phone": @"phone",
             @"fax": @"fax",
             @"email": @"email",
             @"logoURL": @"logo_url",
             @"isOpen": @"close",
             @"minOrderAmount": @"min_order_amount",
             @"avgDeliveryTime": @"avg_deliver_time",
             @"avgPickupTime": @"avg_pickup_time",
             @"deliveryCharges": @"delivery_charges",
             @"priceLevel": @"price_level",
             @"deliveryPostalCode": @"delivery_postal_codes",
             @"okRating": @"rating_okay",
             @"likeRating": @"rating_liked",
             @"loveRating": @"rating_loved",
             @"isDelivery": @"1",
             @"isPickup": @"1",
             @"totalRevenue": @"total_revenue",
             @"totalOrders": @"total_orders",
             @"hasDeal": @"has_deal",
             @"userRating": @"user_rating",
             @"isUserFollowing": @"user_following"
             };
}

+ (NSValueTransformer *)logoURLJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)isUserFollowingJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *following) {
        return @([@"Following" isEqualToString:following]);
    }];
}

@end
