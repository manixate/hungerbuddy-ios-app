//
//  Order.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/9/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FoodItem;

@interface Order : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic) int orderID;
@property (nonatomic) int menuID;
@property (nonatomic, strong) NSString *menuName;
@property (nonatomic) int menuSizeID;
@property (nonatomic, strong) NSString *menuSize;
@property (nonatomic) int menuMeatID;
@property (nonatomic, strong) NSString *menuMeat;
@property (nonatomic) float sizePrice;
@property (nonatomic) float meatPrice;
@property (nonatomic) int quantity;
@property (nonatomic, strong) NSString *spicy;
@property (nonatomic) float price;
@property (nonatomic) float totalPrice;

@property (nonatomic, strong) NSString *note;

+ (id)orderFromFoodItem:(FoodItem *)item;
- (float)getOrderTotalPrice;

@end
