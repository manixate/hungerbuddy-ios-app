//
//  FoodItem.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DishSize.h"
#import "DishMeat.h"
#import "DishSpice.h"

@interface FoodItem : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic) int restaurantID;
@property (nonatomic) int menuTypeID;
@property (nonatomic) int menuCategoryID;
@property (nonatomic, strong) NSString *menuName;
@property (nonatomic, strong) NSString *foodItemDescription;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic) float price;
@property (nonatomic) float ratingOk;
@property (nonatomic) float ratingLiked;
@property (nonatomic) float ratingLoved;
@property (nonatomic) float ratingAll;
@property (nonatomic, strong) NSString *spiceOption;
@property (nonatomic, strong) NSString *chefsSpecial;
@property (nonatomic, strong) NSString *vegetarian;
@property (nonatomic, strong) NSString *sizes;
@property (nonatomic, strong) NSArray *dishSizes;
@property (nonatomic, strong) NSArray *dishMeats;
@property (nonatomic, strong) NSArray *dishSpices;
@property (nonatomic) float userRating;

@property (nonatomic, strong) NSString *note;

@property (nonatomic) int currentDishSizeIdx;
@property (nonatomic) int currentDishMeatIdx;
@property (nonatomic) int currentDishSpiceIdx;

- (DishSize *) currentSize;
- (DishMeat *) currentMeat;
- (DishSpice *) currentSpice;

@end
