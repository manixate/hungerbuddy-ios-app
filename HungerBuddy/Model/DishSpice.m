//
//  DishSpice.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 22/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "DishSpice.h"

@implementation DishSpice
@synthesize spice;

- (id)init {
    if (self = [super init]) {
        spice = @"";
    }
    
    return self;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"spice": @"spice",
             };
}

@end
