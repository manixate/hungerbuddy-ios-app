//
//  Restaurant.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/9/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Order.h"

@interface Restaurant : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic) int userID;
@property (nonatomic) int categoryID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) NSString *restaurantName;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *address2;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *fax;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSURL *logoURL;
@property (nonatomic, strong) UIImage *logo;
@property (nonatomic) BOOL isOpen;
@property (nonatomic) float minOrderAmount;
@property (nonatomic) float avgDeliveryTime;
@property (nonatomic) float avgPickupTime;
@property (nonatomic) float deliveryCharges;
@property (nonatomic, strong) NSString *priceLevel;
@property (nonatomic, strong) NSString *deliveryPostalCode;
@property (nonatomic) float okRating;
@property (nonatomic) float likeRating;
@property (nonatomic) float loveRating;
@property (nonatomic) BOOL isDelivery;
@property (nonatomic) BOOL isPickup;
@property (nonatomic, strong) NSDate *dateAdded;
@property (nonatomic) float totalRevenue;
@property (nonatomic) float totalOrders;
@property (nonatomic) BOOL hasDeal;
@property (nonatomic) float userRating;
@property (nonatomic) BOOL isUserFollowing;

@end
