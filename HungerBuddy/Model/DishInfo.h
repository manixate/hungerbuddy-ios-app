//
//  DishInfo.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 8/30/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DishInfo : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic) int restaurantMenuID;
@property (nonatomic) float price;
@property (nonatomic, strong) NSString *dishInfoDescription;
@property (nonatomic) BOOL isDefault;
@property (nonatomic) int sequenceNo;

@end
