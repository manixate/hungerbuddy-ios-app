//
//  Deal.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 16/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Deal : MTLModel<MTLJSONSerializing>

@property (nonatomic) int ID;
@property (nonatomic, strong) NSString *promoCode;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *expiryDate;
@property (nonatomic, strong) NSString *dealDescription;
@property (nonatomic) int restaurantID;
@property (nonatomic, strong) NSString *restaurantName;
@property (nonatomic, strong) NSString *restaurantAddress;
@property (nonatomic, strong) NSURL *restaurantImageURL;

@end
