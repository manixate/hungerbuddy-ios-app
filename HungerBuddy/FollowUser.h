//
//  Follower.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 24/04/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>

@class User;

@interface FollowUser : MTLModel<MTLJSONSerializing>

@property (nonatomic) int followerID;
@property (nonatomic, strong) User *user;

@end
