//
//  DealsDetailViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 12/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "DealsDetailViewController.h"
#import "MPFlipTransition.h"
#import "Deal.h"
#import "AFNetworking.h"
#import <FacebookSDK/FacebookSDK.h>
#import "ActivityIndicator.h"

@interface DealsDetailViewController () {
    NSTimer *minuteTimer;
    
    NSInteger minutes, hours, days;
    
    Deal *deal;
}

@end

@implementation DealsDetailViewController

- (id)initWithDeal:(Deal *)aDeal
{
    self = [super initWithNibName:@"DealsDetailViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        deal = aDeal;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    

    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kButtonTypeNone];
    
    [self fillData];
}

- (void)viewDidUnload {
    [self invalidateTimers];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}


#pragma mark - Own Methods
- (void)fillData {
    self.restaurantLbl.text = deal.restaurantName;
    self.restaurantAddressLbl.text = deal.restaurantAddress;
    [self.restaurantImageView setImageWithURL:deal.restaurantImageURL];
    self.descriptionTextView.text = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEEE MMM, dd yyyy";
    
    NSDate *startDate = deal.startDate;
    NSDate *endDate = deal.expiryDate;
    
    if (!startDate)
        startDate = [NSDate date];
    if (!endDate)
        endDate = [NSDate date];
    
    self.timeLbl.text = [dateFormatter stringFromDate:endDate];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit
                                               fromDate:startDate
                                                 toDate:endDate
                                                options:0];
    
    days = components.day;
    hours = components.hour;
    minutes = components.minute;
    
    int total = (days * 24 * 3600) + (hours * 3600) + (minutes * 60);
    
    // TODO: Handle expired deal
    if (total >= 0)
        [self startAnimatingTickers];
}

- (void)startAnimatingTickers
{
    [self invalidateTimers];
    
    if (!minuteTimer || ![minuteTimer isValid])
        minuteTimer = [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(animateMinutes) userInfo:nil repeats:YES];
    
    [minuteTimer fire];
    
    [self.daysView addSubview:[self getLabelWithValue:days description:@"Days" andContainer:self.daysView]];
    [self.hoursView addSubview:[self getLabelWithValue:hours description:@"Hours" andContainer:self.hoursView]];
    [self.minutesView addSubview:[self getLabelWithValue:minutes description:@"Minutes" andContainer:self.minutesView]];
}

- (void)invalidateTimers {
    if ([minuteTimer isValid])
        [minuteTimer invalidate];
    
    minuteTimer = nil;
}

- (UIView *)getLabelWithValue:(NSInteger)value description:(NSString *)text andContainer:(UIView *)container
{
    UIView *containerView = [[UIView alloc] initWithFrame:container.bounds];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TickerBG.png"]];
    imageView.frame = containerView.bounds;
    
    UILabel *descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 58, containerView.bounds.size.width, 21)];
    descriptionLabel.textAlignment = NSTextAlignmentCenter;
    descriptionLabel.font = [UIFont systemFontOfSize:14];
    descriptionLabel.text = text;
    
	UILabel *label = [[UILabel alloc] initWithFrame:containerView.bounds];
	label.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
	[label setFont:[UIFont boldSystemFontOfSize:40]];
	label.textAlignment = NSTextAlignmentCenter;
	[label setTextColor:[UIColor grayColor]];
	label.text = [NSString stringWithFormat:@"%02ld", value];
    
    label.backgroundColor = [UIColor clearColor];
	
	label.tag = index;
    
    [containerView addSubview:imageView];
    [containerView addSubview:label];
    [containerView addSubview:descriptionLabel];
    
    containerView.tag = 20;
	
	return containerView;
}

- (void)animateDays {
    NSLog(@"Animating days %02ld", days);
    
    if (days <= 0) {
        [self invalidateTimers];
    }
    
    UIView *previousLabel = [self.daysView viewWithTag:20];
    UIView *nextLabel = [self getLabelWithValue:days description:@"Days" andContainer:self.daysView];
    
    [MPFlipTransition transitionFromView:previousLabel toView:nextLabel duration:0.5 style:MPFlipStyleOrientationVertical | MPFlipStyleDirectionBackward transitionAction:MPTransitionActionAddRemove completion:nil];
}

- (void)animateHours {
    NSLog(@"Animating hours %02ld", hours);
    
    if (hours <= 0) {
        days--;
        hours = 23;
        
        [self animateDays];
    } else {
        hours--;
    }
    
    UIView *previousLabel = [self.hoursView viewWithTag:20];
    UIView *nextLabel = [self getLabelWithValue:hours description:@"Hours" andContainer:self.hoursView];
    
    [MPFlipTransition transitionFromView:previousLabel toView:nextLabel duration:0.5 style:MPFlipStyleOrientationVertical | MPFlipStyleDirectionBackward transitionAction:MPTransitionActionAddRemove completion:nil];
}

- (void)animateMinutes {
    NSLog(@"Animating minutes %02ld", minutes);
    
    if (minutes <= 0) {
        hours--;
        minutes = 59;
        
        [self animateHours];
    } else {
        minutes--;
    }
    
    UIView *previousLabel = [self.minutesView viewWithTag:20];
    UIView *nextLabel = [self getLabelWithValue:minutes description:@"Minutes" andContainer:self.minutesView];
    
    [MPFlipTransition transitionFromView:previousLabel toView:nextLabel duration:0.5 style:MPFlipStyleOrientationVertical | MPFlipStyleDirectionBackward transitionAction:MPTransitionActionAddRemove completion:nil];
}

- (void)shareDeal:(int)dealID
{
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection,
       NSDictionary<FBGraphUser> *user,
       NSError *error) {
         if (error) {
             NSLog(@"FB Login Error: %@", error);
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             
             [Utility showAlert:@"Cannot fetch your facebook information."];
             
             return;
         }
         
         AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
         manager.requestSerializer = [AFJSONRequestSerializer serializer];
         
         NSString *url = [NSString stringWithFormat:@"%@%@", URL_BASE, DEAL_SHARE_FB];
         
         NSDictionary *params = @{@"deal_id": @(dealID), @"facebook_id": user.objectID, @"facebook_token": FBSession.activeSession.accessTokenData.accessToken};
         
         [manager POST:url
            parameters:params
               success:^(AFHTTPRequestOperation *operation, id responseObject)
          {
              NSLog(@"JSON: %@", responseObject);
              NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
              
              [indicator stopAnimating];
              [self.navigationItem enableNavigationItem];
              
              if (!responseObject)
              {
                  NSLog(@"Got NULL response from: %@", operation.request.URL);
                  
                  [Utility showAlert:@"Could not share deal. Please try again."];
                  return;
              }
              
              NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
              
              NSString *promoCode = [json objectForKey:@"promo_code"];
              
              self.promoCodeLbl.text = promoCode;
              
          } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"error: %@", operation.responseString);
              NSLog(@"%ld", (long)operation.response.statusCode);
              [Utility showAlert:@"Could not share deal. Please try again."];
              
              [indicator stopAnimating];
              [self.navigationItem enableNavigationItem];
          }];
     }];
}

#pragma mark - Actions

- (IBAction)shareBtnPressed:(id)sender {
    if (!FBSession.activeSession.isOpen)
    {
        [FBSession openActiveSessionWithReadPermissions:Nil allowLoginUI:NO completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
            [self shareDeal:deal.ID];
        }];
    }
    else
    {
        [self shareDeal:deal.ID];
    }
}
@end
