//
//  SocialFeedItem.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 20/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SocialFeedItem.h"

@implementation SocialFeedItem

- (id)init
{
    self = [super init];
    if (self) {
        self.comments = [NSArray array];
    }
    return self;
}

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"postID": @"id",
             @"user": @"user",
             @"imageURL": @"image",
             @"feedDescription": @"description",
             @"dateAdded": @"date_added",
             @"timeElapsed": @"time_elapsed",
             @"comments": @"comments"
             };
}

+ (NSValueTransformer *)userJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:User.class];
}

+ (NSValueTransformer *)commentsJSONTransformer
{
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:Comment.class];
}

+ (NSValueTransformer *)imageURLJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end

@implementation Comment

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"commentID": @"id",
             @"commentPostID": @"post_id",
             @"imageURL": @"com_profile_pic",
             @"user": @"user",
             @"commentDescription": @"description",
             @"commentDateAdded": @"date_added",
             @"timeElapsed": @"time_elapsed"
             };
}

+ (NSValueTransformer *)imageURLJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)userJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:User.class];
}

@end