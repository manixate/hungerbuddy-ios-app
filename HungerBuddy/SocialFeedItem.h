//
//  SocialFeedItem.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 20/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface SocialFeedItem : MTLModel<MTLJSONSerializing>

@property (nonatomic) int postID;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) NSString *feedDescription;
@property (nonatomic, strong) NSString *dateAdded;
@property (nonatomic, strong) NSString *timeElapsed;
@property (nonatomic, strong) NSArray *comments;

@end

@interface Comment : MTLModel<MTLJSONSerializing>

@property (nonatomic) int commentID;
@property (nonatomic) int commentPostID;
@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, strong) User *user;
@property (nonatomic, strong) NSString *commentDescription;
@property (nonatomic, strong) NSString *commentDateAdded;
@property (nonatomic, strong) NSString *timeElapsed;

@end