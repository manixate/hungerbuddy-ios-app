//
//  SplitUserViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 22/02/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SplitUserViewControllerDelegate <NSObject>
- (void)splitUsersListConfirmed:(NSArray *)splitArray andMinutes:(NSInteger)aMinutes;
@end

@interface SplitUserViewController : UIViewController

@property (nonatomic, weak) id<SplitUserViewControllerDelegate> delegate;

- (id)initWithSplitArray:(NSArray *)splitArray totalAmount:(double)aTotalAmount andMinutes:(NSInteger)aMinutes;

@end
