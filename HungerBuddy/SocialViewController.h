//
//  SocialViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 12/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@class User;

@interface SocialViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    NSArray *feeds;
    UIImage *postImage;
    
    User *user;
}

- (id)initWithUser:(User *)aUser;

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userLovedCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *userFollowingCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *userFollowersCountLbl;
@property (weak, nonatomic) IBOutlet UIImageView *followingImageView;
@property (weak, nonatomic) IBOutlet UITableView *socialFeedTableView;
@property (weak, nonatomic) IBOutlet UITextField *noteTextField;

- (IBAction)cameraBtnPressed:(id)sender;
- (IBAction)postNoteBtnPressed:(id)sender;

@end
