//
//  FoodOptionView.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 25/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FoodOptionView.h"
#import "FoodOptionCell.h"
#import "FoodItem.h"

static NSString *cellIdentifier = @"FoodOptionCellIdentifier";

@implementation FoodOptionView

- (id)initWithFoodItem:(FoodItem *)aFoodItem {
    self = [[[NSBundle mainBundle] loadNibNamed:@"FoodOptionView" owner:self options:nil] firstObject];
    if (self) {
        foodItem = aFoodItem;
        
        self.spicyTableView = [[UITableView alloc] initWithFrame:self.spicyView.bounds];
        self.sizeTableView = [[UITableView alloc] initWithFrame:self.sizeView.bounds];
        self.meatTableView = [[UITableView alloc] initWithFrame:self.meatView.bounds];
        
        self.spicyTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.sizeTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        self.meatTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
        
        self.spicyTableView.frame = self.spicyView.bounds;
        self.sizeTableView.frame = self.sizeView.bounds;
        self.meatTableView.frame = self.meatView.bounds;
        
        self.spicyTableView.delegate = self;
        self.spicyTableView.dataSource = self;
        self.spicyTableView.showsVerticalScrollIndicator = false;
        self.spicyTableView.showsHorizontalScrollIndicator = false;
        self.spicyTableView.backgroundColor = [UIColor clearColor];
        self.spicyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.spicyTableView.rowHeight = 60;
        [self.spicyTableView registerNib:[UINib nibWithNibName:@"FoodOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
        
        self.sizeTableView.delegate = self;
        self.sizeTableView.dataSource = self;
        self.sizeTableView.showsVerticalScrollIndicator = false;
        self.sizeTableView.showsHorizontalScrollIndicator = false;
        self.sizeTableView.backgroundColor = [UIColor clearColor];
        self.sizeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.sizeTableView.rowHeight = 60;
        [self.sizeTableView registerNib:[UINib nibWithNibName:@"FoodOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
        
        self.meatTableView.delegate = self;
        self.meatTableView.dataSource = self;
        self.meatTableView.showsVerticalScrollIndicator = false;
        self.meatTableView.showsHorizontalScrollIndicator = false;
        self.meatTableView.backgroundColor = [UIColor clearColor];
        self.meatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.meatTableView.rowHeight = 60;
        [self.meatTableView registerNib:[UINib nibWithNibName:@"FoodOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
        
        [self.spicyView addSubview:self.spicyTableView];
        [self.sizeView addSubview:self.sizeTableView];
        [self.meatView addSubview:self.meatTableView];
        
        currentSpice = foodItem.currentDishSpiceIdx;
        currentSize = foodItem.currentDishSizeIdx;
        currentMeat = foodItem.currentDishMeatIdx;
        
        note = foodItem.note;
    }
    
    return self;
}

- (void)setSelections {
    if (currentSpice >= 0)
        [self.spicyTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentSpice inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    if (currentSize >= 0)
        [self.sizeTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentSize inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    if (currentMeat >= 0)
        [self.meatTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:currentMeat inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)close {
    [self.spicyTableView removeFromSuperview];
    [self.sizeTableView removeFromSuperview];
    [self.meatTableView removeFromSuperview];
    
    [self.delegate close:foodItem];
}

- (IBAction)confirmBtnPressed:(id)sender {
    foodItem.currentDishSpiceIdx = currentSpice;
    foodItem.currentDishSizeIdx = currentSize;
    foodItem.currentDishMeatIdx = currentMeat;
    
#pragma mark - Only save note on confirm
    foodItem.note = note;
    
    [self close];
}

- (IBAction)closeBtnPressed:(id)sender {
    [self close];
}

- (IBAction)addNoteBtnPressed:(id)sender {
    
    YIPopupTextView* popupTextView = [[YIPopupTextView alloc] initWithPlaceHolder:@"Set note" maxCount:1000 buttonStyle:YIPopupTextViewButtonStyleRightCancelAndDone];
    popupTextView.delegate = self;
    popupTextView.caretShiftGestureEnabled = YES;   // default = NO
    popupTextView.text = note;
    
    [popupTextView showInView:self.superview];
}

#pragma mark - YIPopupTextViewDelegate Methods
- (void)popupTextView:(YIPopupTextView *)textView didDismissWithText:(NSString *)text cancelled:(BOOL)cancelled
{
    if (!cancelled)
        note = text;
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.spicyTableView)
        return foodItem.dishSpices.count;
    else if (tableView == self.sizeTableView)
        return foodItem.dishSizes.count;
    else
        return foodItem.dishMeats.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FoodOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (tableView == self.spicyTableView) {
        DishSpice *spice = foodItem.dishSpices[indexPath.row];
        cell.itemTextLbl.text = spice.spice;
        
        if (currentSpice == indexPath.row)
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
        
    } else if (tableView == self.sizeTableView) {
        DishSize *size = foodItem.dishSizes[indexPath.row];
        cell.itemTextLbl.text = [NSString stringWithFormat:@"%@\n$ %0.2f", size.size, size.price];
        
        if (currentSize == indexPath.row)
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    } else {
        DishMeat *meat = foodItem.dishMeats[indexPath.row];
        cell.itemTextLbl.text = [NSString stringWithFormat:@"%@\n$ %0.2f", meat.meat, meat.price];
        
        if (currentMeat == indexPath.row)
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (selectedIndexPath == nil || ![selectedIndexPath isEqual:indexPath]) {
        BOOL isFirst = false;
        
        if (selectedIndexPath == nil)
            isFirst = true;
        
        NSIndexPath *oldIndexPath = selectedIndexPath;
        selectedIndexPath = indexPath;
        
        FoodOptionCell *selectedFoodOptionCell = (FoodOptionCell *)[tableView cellForRowAtIndexPath:selectedIndexPath];
        
        FoodOptionCell *deselectedFoodOptionCell = nil;
        if (!isFirst) {
            deselectedFoodOptionCell = (FoodOptionCell *)[tableView cellForRowAtIndexPath:oldIndexPath];
        }
        
        selectedFoodOptionCell.ringImageView.highlighted = true;
        
        if (deselectedFoodOptionCell)
            deselectedFoodOptionCell.ringImageView.highlighted = false;
        
        if (tableView == self.spicyTableView) {
            currentSpice = indexPath.row;
        } else if (tableView == self.sizeTableView) {
            currentSize = indexPath.row;
        } else {
            currentMeat = indexPath.row;
        }
    }
}

@end
