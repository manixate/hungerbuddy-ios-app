//
//  main.m
//  HungerBuddy
//
//  Created by sajjad ali on 9/25/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
