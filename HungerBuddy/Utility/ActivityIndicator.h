//
//  ActivityIndicator.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 28/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityIndicator : UIView

@property UIActivityIndicatorView *activityIndicatorView;

+ (ActivityIndicator *)activityIndicatorForView:(UIView *)view;
- (void)stopAnimating;

@end
