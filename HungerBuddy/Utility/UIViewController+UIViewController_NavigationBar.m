//
//  UIViewController+UIViewController_NavigationBar.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 05/03/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "UIViewController+UIViewController_NavigationBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIViewController (UIViewController_NavigationBar)

- (void)configureNavBarWithLeftButton:(NBButtonType)leftButtonType titleButton:(NBButtonType)titleButtonType andRightButton:(NBButtonType)rightButtonType
{
    NSDictionary *imagesDict = @{
                                    @(kButtonTypeNone)      : @"",
                                    @(kBackButton)          : @"backArrow.png",
                                    @(kCrossButton)         : @"crossBG.png",
                                    @(kHomeButton)          : @"homeBG.png",
                                    @(kConfirmButton)       : @"confirmIconBG.png",
                                    @(kSortButton)          : @"sortIconBG.png",
                                    @(kSettingsButton)      : @"settingsIconBG.png",
                                    @(kContronPanelButton)  : @"control_panel.png"
                                };
    
    NSString *leftBtnImageName = imagesDict[@(leftButtonType)];
    NSString *titleBtnImageName = imagesDict[@(titleButtonType)];
    NSString *rightBtnImageName = imagesDict[@(rightButtonType)];
    
    [self configureNavBarLeftButton:leftBtnImageName];
    [self configureNavBarTitle:titleBtnImageName];
    [self configureNavBarRightButton:rightBtnImageName];
}

- (void)configureNavBarTitle:(NSString *)imageName
{
    UIImage *controlPanelImage = [UIImage imageNamed:imageName];
    if (!controlPanelImage)
        return;
    
    UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [titleButton setImage:controlPanelImage
                forState:UIControlStateNormal];
    
    titleButton.frame = CGRectMake(0, 0, controlPanelImage.size.width, controlPanelImage.size.height);
    
    SEL titleBtnPressedSelector = @selector(titleBtnPressed:);
    
    if ([self respondsToSelector:titleBtnPressedSelector])
    {
        [titleButton addTarget:self
                       action:titleBtnPressedSelector
             forControlEvents:UIControlEventTouchUpInside];
    }
    
    self.navigationItem.titleView = titleButton;
}

- (void)configureNavBarLeftButton:(NSString *)imageName
{
    UIImage *backButtonImage = [UIImage imageNamed:imageName];
    if (!backButtonImage)
        return;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    backButton.layer.borderWidth = 1.0;
    
    [backButton setImage:backButtonImage
                forState:UIControlStateNormal];
    
    backButton.frame = CGRectMake(0, 0, 49, 44);
    
    SEL backBtnPressedSelector = @selector(backBtnPressed:);
    
    assert([self respondsToSelector:backBtnPressedSelector]);
    
    [backButton addTarget:self
                   action:backBtnPressedSelector
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.navigationItem addLeftBarButtonItem:backBarButtonItem];
}

- (void)configureNavBarRightButton:(NSString *)imageName
{
    UIImage *rightButtonImage = [UIImage imageNamed:imageName];
    if (!rightButtonImage)
        return;
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    rightButton.layer.borderWidth = 1.0;
    
    [rightButton setImage:rightButtonImage
                forState:UIControlStateNormal];
    
    rightButton.frame = CGRectMake(0, 0, 49, 44);
    
    SEL rightBtnPressedSelector = @selector(rightBtnPressed:);
    
    assert([self respondsToSelector:rightBtnPressedSelector]);
    
    [rightButton addTarget:self
                   action:rightBtnPressedSelector
         forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    [self.navigationItem addRightBarButtonItem:rightBarButtonItem];
}

@end
