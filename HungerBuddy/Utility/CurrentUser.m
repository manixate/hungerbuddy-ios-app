//
//  UserInfo.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "CurrentUser.h"
#import "Order.h"
#import <AddressBookUI/AddressBookUI.h>

static CurrentUser *currentUser = nil;

@implementation CurrentUser

@synthesize user, currentReceipt, receipts, currentLocation;

+ (CurrentUser *)getCurrentUser
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        currentUser = [[self alloc] init];
    });
    
    return currentUser;
}

+ (void)setCurrentLocation:(CLLocation *)location
{
    if (!location)
        return;
    
    [CurrentUser getCurrentUser].currentLocation = location;
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    [standardDefaults setObject:@[@(location.coordinate.latitude), @(location.coordinate.longitude)] forKey:CURRENT_LOCATION];
    [standardDefaults synchronize];
}

+ (CLLocation *)getCurrentLocation
{
    if ([CurrentUser getCurrentUser].currentLocation != nil)
        return [CurrentUser getCurrentUser].currentLocation;
    
    NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *temp = [standardDefaults objectForKey:CURRENT_LOCATION];
    if (!temp)
        return nil;
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[temp[0] doubleValue] longitude:[temp[1] doubleValue]];
    
    [CurrentUser getCurrentUser].currentLocation = location;
    
    return [CurrentUser getCurrentUser].currentLocation;
}

+ (User *) getUserInfo
{
    return [CurrentUser getCurrentUser].user;
}

+ (BOOL)isLoggedIn
{
    User *userInfo = [CurrentUser getUserInfo];
    if ([userInfo.userEmail isEqualToString:@""])
        return false;
    
    return true;
}

+ (Receipt *) getCurrentReceipt
{
    return [CurrentUser getCurrentUser].currentReceipt;
}

+ (NSMutableArray *) getReceipts
{
    return [CurrentUser getCurrentUser].receipts;
}

- (id)init {
    if (self = [super init]) {
        user = [[User alloc] init];
        receipts = [[NSMutableArray alloc] init];
        currentReceipt = [[Receipt alloc] init];
    }
    
    return self;
}

+ (void)saveCurrentUser
{
    User *user = [CurrentUser getCurrentUser].user;
    if (!user)
        return;
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSData *userData = [NSKeyedArchiver archivedDataWithRootObject:currentUser.user];
    [standardUserDefaults setObject:userData forKey:USER_LOGIN_DATA];
    [standardUserDefaults synchronize];
}

+ (void)loadCurrentUser
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSData *userData = [standardUserDefaults objectForKey:USER_LOGIN_DATA];
    if (userData)
        [CurrentUser getCurrentUser].user = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
}

+ (void)clearCredentials {
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults removeObjectForKey:USER_LOGIN_DATA];
    
    [CurrentUser getCurrentUser].user = [[User alloc] init];
    [CurrentUser getCurrentUser].receipts = [[NSMutableArray alloc] init];
    [CurrentUser getCurrentUser].currentReceipt = [[Receipt alloc] init];
}

+ (NSString *)getDeliveryAddressString
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"No Delivery Address Specified";
    
    return ABCreateStringWithAddressDictionary(deliveryAddressDictionary, YES);
}

+ (NSString *)getDeliveryAddressName
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"";
    
    return deliveryAddressDictionary[ADDRESS_NAME];
}

+ (NSString *)getDeliveryAddressAddress
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"";
    
    return deliveryAddressDictionary[ADDRESS_ADDR];
}

+ (NSString *)getDeliveryAddressCity
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"";
    
    return deliveryAddressDictionary[ADDRESS_CITY];
}

+ (NSString *)getDeliveryAddressState
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"";
    
    return deliveryAddressDictionary[ADDRESS_STATE];
}

+ (NSString *)getDeliveryAddressPostalCode
{
    NSDictionary *deliveryAddressDictionary = [CurrentUser getCurrentUser].deliveryAddressDictionary;
    if (!deliveryAddressDictionary || deliveryAddressDictionary.count == 0)
        return @"";
    
    return deliveryAddressDictionary[ADDRESS_POSTAL_CODE];
}

#warning Just Test Code
- (void)fillTestData
{
    NSMutableArray *tempReceipts = [[NSMutableArray alloc] init];
    for (int i = 0; i < 5; i++) {
        Receipt *receipt = [[Receipt alloc] init];
        
        for (int i = 0; i < 10; i++) {
            Order *order = [[Order alloc] init];
            
            order.ID = i;
            order.orderID = i;
            order.menuID = i;
            order.menuName = [NSString stringWithFormat:@"Order %d", i];
            order.menuSizeID = i;
            order.menuSize = [NSString stringWithFormat:@"Size %d", i];
            order.menuMeatID = i;
            order.menuMeat = [NSString stringWithFormat:@"Meat %d", i];
            order.sizePrice = i;
            order.meatPrice = i;
            order.quantity = rand() % 10;
            order.spicy = @"Non";
            order.totalPrice = (order.sizePrice + order.meatPrice) * order.quantity;
            
            [receipt.orders addObject:order];
        }
        
        [tempReceipts addObject:receipt];
    }
    
    self.receipts = tempReceipts;
    self.currentReceipt = [tempReceipts lastObject];
}

@end
