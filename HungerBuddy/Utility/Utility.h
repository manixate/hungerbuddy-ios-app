//
//  Utility.h
//  SlingTop
//
//  Created by Adnan Nasir on 3/6/13.
//  Copyright (c) 2013 Appstron. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject


+(void) showAlert:(NSString *)msg;
+(NSString *) calculateTimeInterval:(NSString *)targetDate;
+(NSString *) stringByStrippingHTML:(NSString *)s;
+(NSString *) getCurrentDate;
+(NSString *) getFirstDateOfWeek;
+(NSDate *)firstDateOfWeekWithDate:(NSDate *)date;
+ (BOOL)isDefaultAddressPresent;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
