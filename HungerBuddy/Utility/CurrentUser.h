//
//  UserInfo.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "Receipt.h"
#import <CoreLocation/CoreLocation.h>

@interface CurrentUser : NSObject

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) Receipt *currentReceipt;
@property (nonatomic, strong) NSMutableArray *receipts;
@property (nonatomic, strong) CLLocation *currentLocation;
@property (nonatomic, strong) NSDictionary *deliveryAddressDictionary;

+ (User *) getUserInfo;
+ (Receipt *) getCurrentReceipt;
+ (NSMutableArray *) getReceipts;
+ (CLLocation *) getCurrentLocation;
+ (void)setCurrentLocation:(CLLocation *)location;
+ (BOOL)isLoggedIn;

+ (CurrentUser *) getCurrentUser;
+ (void)saveCurrentUser;
+ (void)loadCurrentUser;
+ (void) clearCredentials;

+ (NSString *)getDeliveryAddressString;
+ (NSString *)getDeliveryAddressName;
+ (NSString *)getDeliveryAddressAddress;
+ (NSString *)getDeliveryAddressCity;
+ (NSString *)getDeliveryAddressState;
+ (NSString *)getDeliveryAddressPostalCode;

#warning Just Test Code
- (void) fillTestData;

@end
