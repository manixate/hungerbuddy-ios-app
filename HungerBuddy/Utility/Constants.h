//
//  Constants.h
//  HungerBuddy
//
//  Created by sajjad ali on 9/29/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

//URL
//BASE
//#define URL_BASE @"http://www.hbuddy.seoptoday.com/hungerbuddy/trunk/api/"
#define URL_BASE @"http://hungerbuddy.pakcyber.org/api/"
//Login COmmand
#define CMD_LOGIN @"users/login/id?ajax=true&user_type=3"
//FB Login
#define FB_LOGIN @"users/fb_login/?fb=%@&access_token=%@&email=%@&first_name=%@&last_name=%@&fbid=%@"
//http://hbuddy.seoptoday.com/hungerbuddy/trunk/api/restaurants/?address=B2S
#define RS_SEARCH_NAME @"restaurants/typeahead_name/?term="
#define RS_SEARCH_ZIP @"restaurants/typeahead_address/?term="
#define RS_SEARCH_DISH @"restaurants/dishes/?term="

//USER Profile
#define USER_FOLLOWER_COUNTER @"followers/count?user_id=%d"
#define USER_FOLLOWING_COUNTER @"following/count?user_id=%d"
#define USER_LOVED_COUNTER @"restaurants/loved_count?user_id=%d"

// Recent orders
#define USER_RECENT_ORDERS @"orders/index/id?user_id="

// Get users
#define GET_ALL_USERS @"users/?user_type=3"
#define GET_FOLLOWERSLIST @"followers/index/id?f_listings=yes&type=%@&user_id=%d"

// Categories List
#define CATEGORIES_URL @"restaurant_categories/"

// Restaurants List
#define RES_CAT_ULR @"restaurants/?category_id="

// Restaurant Info
#define RES_INFO @"restaurants/?id=%d"
#define RES_FOLLOW @"restaurant_followers/follow/?restaurant_id=%d"
#define RES_UNFOLLOW @"restaurant_followers/unfollow/?restaurant_id=%d"

// Food Category List
#define RES_FOOD_CAT_URL @"restaurant_menu_categories/index/id?restaurant_id="

// Food List
#define FOOD_LIST_URL @"restaurant_menus/index/id?category_id=%d&restaurant_id=%d"

// Social
#define NEWS_FEED_URL @"posts/index/?type=news"
#define USER_WALL_POSTS_URL @"posts/index/?type=wall&wall_user_id=%d"
#define POST_IMAGE_BASE_URL @"../application/files/post_images/"
#define COMMENT_URL @"comments/index/"

// Deals
#define ALL_DEALS_URL @"deals/index/id"
#define DEAL_DETAIL_URL @"deals/index/id/%d"
#define RESTAURANT_IMAGE_URL @"../application/files/restaurants_images/"
#define DEAL_SHARE_FB @"deals/shareToFacebook/"

// Order
#define ORDER_UPLOAD_URL @"orders/addAllMenuItems/"
#define ORDER_DETAIL_URL @"orders/status/?order_id=%d&token=%@"

// Edit User Info
#define EDIT_USER_INFO_URL @"users/change_contact_info/"

//VIEW BASED COMPONENTS TAGS
//Login View
#define TAG_LOGIN_USERNAME 1
#define TAG_LOGIN_PASSWORD 2

//ALERT MESSAGE
#define ALERT_RESTURANTS_ZERO @"No Resturant found"

// Other Constants
#define RESTAURANT_ROW_HEIGHT 60
#define RECIPE_ROW_HEIGHT 60
#define FOODITEM_ROW_HEIGHT 88
#define FEEDITEM_ROW_HEIGHT 158
#define FOLLOWER_ROW_HEIGHT 50

#define CURRENT_LOCATION @"CurrentLocation"
#define DEFAULT_ADDRESS @"DefaultAddress"

// UserLogin Info
#define USER_LOGIN_DATA @"UserLoginData"
#define FB_SESSION_DATA @"FBSessionData"
#define TWITTER_SESSION_DATA @"TwitterSessionData"

#define ADDRESS_NAME @"Name"
#define ADDRESS_ADDR @"Thoroughfare"
#define ADDRESS_STATE @"State"
#define ADDRESS_CITY @"City"
#define ADDRESS_POSTAL_CODE @"PostalCode"

#define FOLLOWING @"following"
#define FOLLOWERS @"followers"