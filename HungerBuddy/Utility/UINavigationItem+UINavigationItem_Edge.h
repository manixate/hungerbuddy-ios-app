//
//  UINavigationItem+UINavigationItem_Edge.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 05/03/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationItem (UINavigationItem_Edge)

- (void)addLeftBarButtonItem:(UIBarButtonItem *)leftBarButtonItem;
- (void)addRightBarButtonItem:(UIBarButtonItem *)rightBarButtonItem;

- (void)disableNavigationItem;
- (void)enableNavigationItem;

- (void)disableLeftButton;
- (void)disableTitleButton;
- (void)disableRightButton;

- (void)enableLeftButton;
- (void)enableTitleButton;
- (void)enableRightButton;

@end
