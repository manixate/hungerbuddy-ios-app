//
//  UIViewController+UIViewController_NavigationBar.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 05/03/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

//typedef NS_ENUM(NSUInteger, NBOptions) {
//    kNBShowControlPanel = 1UL << 0,
//    kNBShowBackArrow = 1UL << 1,
//    kNBShowCross = 1UL << 2,
//    kNBShowHome = 1UL << 3,
//    kNBShowConfirm = 1UL << 4,
//    kNBShowSortIcon = 1UL << 5,
//    kNBShowSettingsIcon = 1UL << 6,
//};

typedef NS_ENUM(NSUInteger, NBButtonType) {
    kButtonTypeNone,
    kBackButton,
    kCrossButton,
    kHomeButton,
    kConfirmButton,
    kSortButton,
    kSettingsButton,
    kContronPanelButton
};

@interface UIViewController (UIViewController_NavigationBar)

- (void)configureNavBarWithLeftButton:(NBButtonType)leftButtonType titleButton:(NBButtonType)titleButtonType andRightButton:(NBButtonType)rightButtonType;

//- (void)configureNavBarWithOptions:(NBOptions)navBarOptions;

- (void)configureNavBarTitle:(NSString *)imageName;
- (void)configureNavBarLeftButton:(NSString *)imageName;
- (void)configureNavBarRightButton:(NSString *)imageName;

@end
