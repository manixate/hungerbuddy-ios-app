//
//  ModalView.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 10/12/2013.
//  Copyright (c) 2013 Seven Koncepts. All rights reserved.
//

#import "ModalView.h"

@interface ModalView () {
    __weak UIView *mainView;
    
    UIView *backgroundView;
}

@end

@implementation ModalView

- (id)initWithSuperView:(UIView *)aView andModalView:(UIView *)aModalView
{
    self = [super initWithFrame:aView.bounds];
    if (self) {
        mainView = aView;
        self.modalView = aModalView;
        
        backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        backgroundView.userInteractionEnabled = true;
        [self addSubview:backgroundView];

        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gestureAction:)];
        [backgroundView addGestureRecognizer:gesture];
        
        [self.modalView setCenter:self.center];
        [self addSubview:self.modalView];
        
    }
    return self;
}

- (void)gestureAction:(UITapGestureRecognizer *)gesture {
    CGPoint location = [gesture locationInView:self];
    if (!CGRectContainsPoint(self.modalView.frame, location))
        [self hideModalView];
}

- (void)showModalView {
    if ([self.delegate respondsToSelector:@selector(modalViewShouldAppear:)] &&
        ![self.delegate modalViewShouldAppear:self])
    {
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(modalViewWillAppear:)])
        [self.delegate modalViewWillAppear:self];
    
    self.layer.opacity = 0.0;
    
    [self.superview endEditing:YES];
    
    [self addSubview:self.modalView];
    [mainView addSubview:self];
    
    [UIView animateWithDuration:0.2f animations:^{
        self.layer.opacity = 1.0;
    } completion:^(BOOL finished) {
        [self.delegate modalViewDidAppear:self];
    }];
}

- (void)hideModalView {
    if ([self.delegate respondsToSelector:@selector(modalViewShouldDisappear:)] &&
        ![self.delegate modalViewShouldDisappear:self])
    {
        return;
    }
    
    if ([self.delegate respondsToSelector:@selector(modalViewWillDisappear:)])
        [self.delegate modalViewWillDisappear:self];
    
    [UIView animateWithDuration:0.2f animations:^{
        self.layer.opacity = 0.0;
    } completion:^(BOOL finished) {
        [self.modalView removeFromSuperview];
        [self removeFromSuperview];
        
        [self.delegate modalViewDidDisappear:self];
    }];
}

@end
