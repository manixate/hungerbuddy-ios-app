//
//  ActivityIndicator.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 28/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ActivityIndicator.h"

@implementation ActivityIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (ActivityIndicator *)activityIndicatorForView:(UIView *)view {
    ActivityIndicator *indicator = [[ActivityIndicator alloc] initWithFrame:view.bounds];
    indicator.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
    indicator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    indicator.activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    indicator.activityIndicatorView.frame = indicator.bounds;
    indicator.activityIndicatorView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [indicator addSubview:indicator.activityIndicatorView];
    [indicator.activityIndicatorView startAnimating];
    
    [view addSubview:indicator];
    
    return indicator;
}

- (void)stopAnimating {
    [self.activityIndicatorView stopAnimating];
    [self.activityIndicatorView removeFromSuperview];
    
    [self removeFromSuperview];
}

@end
