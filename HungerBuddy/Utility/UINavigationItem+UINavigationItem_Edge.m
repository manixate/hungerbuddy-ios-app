//
//  UINavigationItem+UINavigationItem_Edge.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 05/03/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "UINavigationItem+UINavigationItem_Edge.h"

#define EDGE_WIDTH_IOS7 -16
#define EDGE_WIDTH_IOS6 -6;

@implementation UINavigationItem (UINavigationItem_Edge)

- (void)addLeftBarButtonItem:(UIBarButtonItem *)leftBarButtonItem
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        negativeSpacer.width = EDGE_WIDTH_IOS7;
    } else {
        negativeSpacer.width = EDGE_WIDTH_IOS6;
    }
    
    [self setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, leftBarButtonItem, nil]];
}

- (void)addRightBarButtonItem:(UIBarButtonItem *)rightBarButtonItem
{
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0) {
        negativeSpacer.width = EDGE_WIDTH_IOS7;
    } else {
        negativeSpacer.width = EDGE_WIDTH_IOS6;
    }
    
    [self setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer, rightBarButtonItem, nil]];
}

- (void)disableNavigationItem
{
    [self disableLeftButton];
    [self disableTitleButton];
    [self disableRightButton];
}

- (void)enableNavigationItem
{
    [self enableLeftButton];
    [self enableTitleButton];
    [self enableRightButton];
}

- (void)disableLeftButton
{
    UIView *item = self.leftBarButtonItems.lastObject;
    
    if (item && [item isKindOfClass:[UIBarButtonItem class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = false;
    }
}

- (void)disableTitleButton
{
    UIView *item = self.titleView;
    
    if (item && [item isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = false;
    }
}

- (void)disableRightButton
{
    UIView *item = self.rightBarButtonItems.lastObject;
    
    if (item && [item isKindOfClass:[UIBarButtonItem class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = false;
    }
}

- (void)enableLeftButton
{
    UIView *item = self.leftBarButtonItems.lastObject;
    
    if (item && [item isKindOfClass:[UIBarButtonItem class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = true;
    }
}

- (void)enableTitleButton
{
    UIView *item = self.titleView;
    
    if (item && [item isKindOfClass:[UIButton class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = true;
    }
}

- (void)enableRightButton
{
    UIView *item = self.rightBarButtonItems.lastObject;
    
    if (item && [item isKindOfClass:[UIBarButtonItem class]])
    {
        UIButton *button = (UIButton *)item;
        
        button.enabled = true;
    }
}

@end
