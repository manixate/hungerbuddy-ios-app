//
//  ModalView.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 10/12/2013.
//  Copyright (c) 2013 Seven Koncepts. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ModalView;
@protocol ModalViewDelegate <NSObject>

@optional
- (BOOL)modalViewShouldAppear:(ModalView *)aModalView;
- (void)modalViewWillAppear:(ModalView *)aModalView;

- (BOOL)modalViewShouldDisappear:(ModalView *)aModalView;
- (void)modalViewWillDisappear:(ModalView *)aModalView;

@required
- (void)modalViewDidAppear:(ModalView *)aModalView;
- (void)modalViewDidDisappear:(ModalView *)aModalView;

@end

@interface ModalView : UIView

@property (nonatomic, weak) id<ModalViewDelegate> delegate;
@property (nonatomic, weak) UIView *modalView;

- (id)initWithSuperView:(UIView *)aView andModalView:(UIView *)aModalView;

- (void)showModalView;
- (void)hideModalView;

@end
