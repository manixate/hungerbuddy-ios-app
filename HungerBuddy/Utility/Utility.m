//
//  Utility.m
//  SlingTop
//
//  Created by Adnan Nasir on 3/6/13.
//  Copyright (c) 2013 Appstron. All rights reserved.
//

#import "Utility.h"

@implementation Utility


+(void) showAlert:(NSString *)msg
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:msg delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil];
    [alert show];
}

+(NSString *) getCurrentDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    
    //[df setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *todayDate = [NSDate date];
    NSString *dateString = [df stringFromDate:todayDate];
    NSArray *dateComponents = [dateString componentsSeparatedByString:@"-"];
    
    return [NSString stringWithFormat:@"%@d%@d%@",[dateComponents objectAtIndex:0], [dateComponents objectAtIndex:1], [dateComponents objectAtIndex:2]];
}

+ (NSDate *)firstDateOfWeekWithDate:(NSDate *)date {
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *weekdayComponents = [calendar components:(NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
    
    NSDateComponents *components = [calendar components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit) fromDate:date];
    [components setDay:-((([weekdayComponents weekday] - [calendar firstWeekday]) + 5 ) % 7)];
    
    return [calendar dateFromComponents:components];
}

+(NSString *) getFirstDateOfWeek
{
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    
    //[df setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDate *todayDate = [NSDate date];
    NSDate *firstDay = [self firstDateOfWeekWithDate:todayDate];
    NSString *dateString = [df stringFromDate:firstDay];
    NSArray *dateComponents = [dateString componentsSeparatedByString:@"-"];
    
    return [NSString stringWithFormat:@"%@d%@d%@",[dateComponents objectAtIndex:0], [dateComponents objectAtIndex:1], [dateComponents objectAtIndex:2]];
}


+(NSString *) calculateTimeInterval:(NSString *)targetDate
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    
    //[df setDateFormat:@"EEE, dd MMM yy HH:mm:ss VVVV"];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *convertedDate = [df dateFromString:targetDate];
   
    NSDate *todayDate = [NSDate date];
    double ti = [convertedDate timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    if(ti < 1)
    {
        return @"";
    }
    else if (ti < 60)
    {
        return @"less than a minute ago";
    }
    else if (ti < 3600)
    {
        int diff = round(ti / 60);
        if(diff==1)
            return[NSString stringWithFormat:@"%d minute ago", diff];
        else
            return [NSString stringWithFormat:@"%d minute(s) ago", diff];
        
    }
    else if (ti < 86400)
    {
        int diff = round(ti / 60 / 60);
        if(diff==1)
            return[NSString stringWithFormat:@"%d hour ago", diff];
        else
            return[NSString stringWithFormat:@"%d hours ago", diff];
    }
    else if (ti < 2629743)
    {
        int diff = round(ti / 60 / 60 / 24);
        if(diff==1)
            return[NSString stringWithFormat:@"%d day ago", diff];
        else
            return[NSString stringWithFormat:@"%d days ago", diff];
    }
    else if (ti < 78892290)
    {
        int diff = round(ti / 60 / 60 / 24/30);
        if(diff==1)
            return[NSString stringWithFormat:@"%d month ago", diff];
        else
            return[NSString stringWithFormat:@"%d months ago", diff];
    }
    else
    {
        return @"a year ago";
    }
}

+(NSString *) stringByStrippingHTML:(NSString *)s
{
    NSRange r;
    //NSString *s = [[self copy] autorelease];
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
    
}

+ (BOOL)isDefaultAddressPresent
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:DEFAULT_ADDRESS] != nil;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    
    UIImage * newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

@end
