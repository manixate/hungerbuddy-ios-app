//
//  AppDelegate.h
//  HungerBuddy
//
//  Created by sajjad ali on 9/25/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HomeViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) HomeViewController *viewController;
@property (strong, nonatomic) NSArray *locations;

@end
