//
//  SplitUserCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 11/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SplitUserCellDelegate <NSObject>

- (void)amountChanged:(float)amount forIndex:(int)index;

@end

@interface SplitUserCell : UITableViewCell {
    NSString *tempText;
}

@property (weak, nonatomic) id<SplitUserCellDelegate> delegate;
@property (nonatomic) int index;

@property (weak, nonatomic) IBOutlet UIView *currentUserView;
@property (weak, nonatomic) IBOutlet UIView *simpleUserView;

@property (weak, nonatomic) IBOutlet UILabel *indexLbl;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailAddressLbl;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;

- (void)setupKeyboard;
- (void)refreshView;

@end
