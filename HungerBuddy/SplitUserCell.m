//
//  SplitUserCell.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 11/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SplitUserCell.h"

@implementation SplitUserCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        [self setupKeyboard];
        
        self.simpleUserView.backgroundColor = [UIColor clearColor];
        self.currentUserView.backgroundColor = [UIColor clearColor];
        
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupKeyboard];
        
        self.simpleUserView.backgroundColor = [UIColor clearColor];
        self.currentUserView.backgroundColor = [UIColor clearColor];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setupKeyboard
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.amountTextField.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    self.amountTextField.text = tempText;
    
    [self.amountTextField resignFirstResponder];
    
    tempText = @"";
}

-(void)doneWithNumberPad{
    NSString *amountText = self.amountTextField.text;

    amountText = [amountText stringByReplacingOccurrencesOfString:@"$" withString:@""];
    amountText = [amountText stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    float value = [amountText floatValue];
    
    [self.delegate amountChanged:value forIndex:self.index];
    
    [self.amountTextField resignFirstResponder];
    
    tempText = @"";
}

- (void)refreshView {
    if (self.index == 0) {
        self.currentUserView.hidden = false;
        self.simpleUserView.hidden = true;
    } else {
        self.currentUserView.hidden = true;
        self.simpleUserView.hidden = false;
    }
}

- (IBAction)editingBegan:(UITextField *)sender {
    NSString *text = sender.text;
    
    text = [text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    sender.text = [NSString stringWithFormat:@"%@", text];
    
    tempText = text;
}

- (IBAction)editingEnded:(UITextField *)sender {
    NSString *text = sender.text;
    
    text = [text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    sender.text = [NSString stringWithFormat:@"$ %@", text];
}

@end
