//
//  SocialDetailViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 12/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SocialDetailViewController.h"

@interface SocialDetailViewController ()

@end

@implementation SocialDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kHomeButton];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
