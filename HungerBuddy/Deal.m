//
//  Deal.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 16/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "Deal.h"

@implementation Deal

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"ID": @"id",
             @"startDate": @"start_time",
             @"expiryDate": @"end_time",
             @"restaurantID": @"restaurant_id",
             @"restaurantName": @"restaurant.name",
             @"restaurantAddress": @"restaurant.address",
             @"restaurantImageURL": @"restaurant.logo",
             @"dealDescription": @"description"
             };
}

+ (NSDateFormatter *)dateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    dateFormatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    return dateFormatter;
}

+ (NSValueTransformer *)startDateJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)expiryDateJSONTransformer
{
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^(NSString *str) {
        return [self.dateFormatter dateFromString:str];
    } reverseBlock:^(NSDate *date) {
        return [self.dateFormatter stringFromDate:date];
    }];
}

+ (NSValueTransformer *)restaurantImageURLJSONTransformer
{
    return [MTLValueTransformer transformerWithBlock:^id(NSString *urlString) {
        NSURL *restaurantImageBaseURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", URL_BASE, RESTAURANT_IMAGE_URL]];
        
        return [NSURL URLWithString:urlString relativeToURL:restaurantImageBaseURL];
    }];
}

@end
