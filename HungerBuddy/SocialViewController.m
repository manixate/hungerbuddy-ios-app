//
//  SocialViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 12/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SocialViewController.h"
#import "SocialFeedItemCell.h"
#import "SocialFeedItem.h"
#import "CurrentUser.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"
#import "SocialFeedItem.h"
#import "MZFormSheetController.h"
#import "CommentsViewController.h"

static NSString *cellIdentifier = @"SocialFeedItemCellIdentifier";

@interface SocialViewController () <SocialFeedItemCellDelegate>

@end

@implementation SocialViewController

- (id)init {
    self = [super initWithNibName:@"SocialViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        user = [CurrentUser getUserInfo];
        postImage = nil;
    }
    return self;
}

- (id)initWithUser:(User *)aUser
{
    self = [super initWithNibName:@"SocialViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        user = aUser;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.socialFeedTableView registerNib:[UINib nibWithNibName:@"SocialFeedItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
    
    self.socialFeedTableView.rowHeight = FEEDITEM_ROW_HEIGHT;
    

    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kHomeButton];
    
    [self fillData];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)cameraBtnPressed:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] initWithRootViewController:self];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot find camera on the device." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alertView show];
    }
}

- (IBAction)postNoteBtnPressed:(id)sender {
    NSString *note = self.noteTextField.text;
    
    if (![note isEqualToString:@""]) {
        [self postfromUserID:[[CurrentUser getUserInfo] userID] toUserID:user.userID];
    }
}

#pragma mark - Own Methods
- (void)fillData {
    [self.userImageView setImageWithURL:user.userImageURL];
    self.userNameLbl.text = [user getUserName];
    
#warning if user is following then show following image label
    self.followingImageView.hidden = false;
    
    [self loadData:user.userID];
}

- (void)loadData:(int)userID {
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    NSString *followerCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_FOLLOWER_COUNTER], userID];
    
    NSString *followingCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_FOLLOWING_COUNTER], userID];
    
    NSString *favoriteCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_LOVED_COUNTER], userID];
    
    NSLog(@"User Follower Counter URL: %@", followerCounterURL);
    
    AFHTTPRequestOperation *followerCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:followerCounterURL]]];
    [followerCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            self.userFollowersCountLbl.text = @"-1";
            
            return;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        self.userFollowersCountLbl.text = [json objectForKey:@"count"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [Utility showAlert:@"Error getting user information"];
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    NSLog(@"User Following Counter URL: %@", followingCounterURL);
    AFHTTPRequestOperation *followingCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:followingCounterURL]]];
    [followingCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            self.userFollowingCountLbl.text = @"-1";
            
            return;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        self.userFollowingCountLbl.text = [json objectForKey:@"count"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [Utility showAlert:@"Error getting user information"];
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    AFHTTPRequestOperation *favoriteCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:favoriteCounterURL]]];
    
    [favoriteCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            self.userLovedCountLbl.text = @"-1";
            
            return;
        }
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        self.userLovedCountLbl.text = [json objectForKey:@"count"];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        [Utility showAlert:@"Error getting user information"];
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    AFHTTPRequestOperation *fetchFeedsRequest = [self getFeeds:userID];
    
    NSArray *operations = [AFHTTPRequestOperation batchOfRequestOperations:@[fetchFeedsRequest, followerCounterRequest, followingCounterRequest, favoriteCounterRequest] progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
    } completionBlock:^(NSArray *operations) {
        __block BOOL failed = false;
        [operations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            AFHTTPRequestOperation *operation = obj;
            
            // We got error in one of our operations so show an alert.
            if (operation.error)
            {
                failed = true;
                *stop = true;
            }
        }];
        
        if (failed)
        {
            [Utility showAlert:@"Error getting complete user information"];
        }
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    }];
    
    [[NSOperationQueue mainQueue] addOperations:operations waitUntilFinished:NO];
}

- (AFHTTPRequestOperation *) getFeeds:(int)userID {
    NSString *userWallPostURL = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, NEWS_FEED_URL], userID];
    
    NSLog(@"User wall post URL: %@", userWallPostURL);
    
    NSURLRequest *fetchFeedsRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:userWallPostURL]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:fetchFeedsRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);

        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            feeds = @[];
            [self.socialFeedTableView reloadData];
            
            return;
        }

        NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        feeds = [MTLJSONAdapter modelsOfClass:SocialFeedItem.class fromJSONArray:json error:&error];
        
        [self.socialFeedTableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
    }];

    return operation;
}

- (void)postfromUserID:(int)userID toUserID:(int)aUserID {
    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    NSString *note = self.noteTextField.text;
    
    NSString *postURLString = [NSString stringWithFormat:@"%@%@", URL_BASE, USER_WALL_POSTS_URL];
    
    NSDictionary *params = @{@"post_note": note, @"user_id_written": @(aUserID), @"post_shared": @(NO)};
    
    NSLog(@"Posting note %@", note);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:postURLString parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        if (postImage) {
            NSData *data = UIImagePNGRepresentation(postImage);
            [formData appendPartWithFormData:data name:@"post_image"];
        }
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSLog(@"Successfully posted note %@", responseObject);
        self.noteTextField.text = @"";
        postImage = nil;
        
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error){
        
        NSLog(@"Failed posting note %@", error);
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed posting note." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    }];
}

#pragma mark - UIImagePickerControllerDelegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:^{
        postImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    }];
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return feeds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SocialFeedItemCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
        cell = [[SocialFeedItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.index = indexPath.row;
    cell.delegate = self;
    
    SocialFeedItem *feedItem = feeds[indexPath.row];
    
    if (!feedItem.imageURL) {
        CGRect tempFrame = cell.feedImageView.frame;
        tempFrame.size = CGSizeZero;
        
        cell.feedImageView.frame = tempFrame;
        
        tempFrame = cell.descriptionTextView.frame;
        tempFrame.origin.x = 73;
        tempFrame.size.width = 241;
        cell.descriptionTextView.frame = tempFrame;
    } else {
        CGRect tempFrame = cell.feedImageView.frame;
        tempFrame.size = CGSizeMake(60, 60);
        
        cell.feedImageView.frame = tempFrame;
        
        tempFrame = cell.descriptionTextView.frame;
        tempFrame.origin.x = 146;
        tempFrame.size.width = 168;
        cell.descriptionTextView.frame = tempFrame;
        
        [cell.feedImageView setImageWithURL:feedItem.imageURL];
    }
    
    [cell.userImageView setImageWithURL:feedItem.user.userImageURL];
    cell.feedTime.text = feedItem.timeElapsed;
    cell.userNameLbl.text = feedItem.user.userName;
    cell.descriptionTextView.text = feedItem.feedDescription;
    cell.commentsCountLbl.text = [NSString stringWithFormat:@"(%lu)", feedItem.comments.count];
    
    return cell;
}

#pragma mark - SocialFeedItemCellDelegate Methods
- (void)commentsBtnPressed:(int)index
{
    SocialFeedItem *feedItem = feeds[index];
    
    CommentsViewController *commentsVC = [[CommentsViewController alloc] initWithSocialFeedItem:feedItem];
    
    MZFormSheetController *sheetController = [[MZFormSheetController alloc] initWithViewController:commentsVC];
    sheetController.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsMoveToTop;
    sheetController.shouldDismissOnBackgroundViewTap = YES;
    [sheetController presentAnimated:YES completionHandler:^(UIViewController *presentedFSViewController) {
        
    }];
}

- (void)shareBtnPressed:(int)index
{
    SocialFeedItem *feedItem = feeds[index];
}

@end
