//
//  AppDelegate.m
//  HungerBuddy
//
//  Created by sajjad ali on 9/25/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "CurrentUser.h"
#import "CSVParser.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    HomeViewController *homeViewController = [[HomeViewController alloc] initWithNibName:@"HomeView" bundle:nil];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:homeViewController];
    self.navigationController.navigationBar.translucent = NO;
//    self.navigationController.navigationBarHidden = true;
    self.navigationController.delegate = self;
    
    UIImage *topBarImage = [UIImage imageNamed:@"emptybar.PNG"];
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7.0)
        topBarImage = [Utility imageWithImage:topBarImage scaledToSize:CGSizeMake(topBarImage.size.width, 64)];
    [[UINavigationBar appearance] setBackgroundImage:topBarImage forBarMetrics:UIBarMetricsDefault];
    
    self.window.rootViewController = self.navigationController;
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:true];
    
    [CurrentUser loadCurrentUser];
    
    self.locations = [CSVParser parseCSVIntoArrayOfDictionariesFromFile:[[NSBundle mainBundle] pathForResource:@"locations" ofType:@"csv"] withSeparatedCharacterString:@"," quoteCharacterString:@"\""];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveCookies];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [self loadCookies];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
    [FBAppCall handleDidBecomeActive];

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[FBSession activeSession] close];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                    fallbackHandler:^(FBAppCall *call) {
                        NSLog(@"In fallback handler");
                    }];
}

#pragma mark - UINavigationControllerDelegate Methods
- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController isKindOfClass:[HomeViewController class]] ||
        [viewController isKindOfClass:[LoginViewController class]])
    {
        [navigationController setNavigationBarHidden:YES animated:animated];
    }
    else
    {
        [navigationController setNavigationBarHidden:NO animated:animated];
    }
}

#pragma mark - Own Methods
- (void)saveCookies{
    
    NSData *cookiesData = [NSKeyedArchiver archivedDataWithRootObject: [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject: cookiesData forKey: @"sessionCookies"];
    [defaults synchronize];
    
}

- (void)loadCookies{
    
    NSArray *cookies = [NSKeyedUnarchiver unarchiveObjectWithData: [[NSUserDefaults standardUserDefaults] objectForKey: @"sessionCookies"]];
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in cookies){
        [cookieStorage setCookie: cookie];
    }
    
}

@end
