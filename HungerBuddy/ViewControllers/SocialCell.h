//
//  SocialCell.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialCellProtocol <NSObject>
- (void)socialCellButtonPressed:(int)index;
@end

@interface SocialCell : UITableViewCell

@property (nonatomic) int index;
@property (weak, nonatomic) id<SocialCellProtocol> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *userAddress;
@property (weak, nonatomic) IBOutlet UIButton *userButton;
@end
