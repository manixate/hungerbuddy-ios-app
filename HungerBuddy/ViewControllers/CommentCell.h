//
//  CommentCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 5/21/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *commentDescription;
@property (weak, nonatomic) IBOutlet UILabel *commentUser;
@property (weak, nonatomic) IBOutlet UILabel *commentDateAdded;
@end
