//
//  ReceiptOrderEditCell.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/20/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReceiptVCProtocol;

@interface ReceiptOrderEditCell : UITableViewCell

@property (nonatomic) NSInteger index;
@property (weak, nonatomic) id<ReceiptVCProtocol> delegate;

@property (weak, nonatomic) IBOutlet UILabel *receiptOrderName;
@property (weak, nonatomic) IBOutlet UILabel *receiptOrderQuantity;
- (IBAction)plusBtnPressed:(id)sender;
- (IBAction)minusBtnPressed:(id)sender;
@end
