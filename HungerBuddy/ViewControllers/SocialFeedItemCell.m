//
//  SocialFeedItemCell.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 16/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SocialFeedItemCell.h"

@implementation SocialFeedItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)commentsBtnPressed:(id)sender {
    [self.delegate commentsBtnPressed:self.index];
}

- (IBAction)shareBtnPressed:(id)sender {
    [self.delegate shareBtnPressed:self.index];
}

@end
