//
//  FoodItemCell.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 23/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantDetailViewController.h"

@interface FoodItemCell : UITableViewCell

@property (weak, nonatomic) id<RestaurantDetailVCProtocol> delegate;
@property (strong, nonatomic) NSIndexPath *indexPath;

@property (weak, nonatomic) IBOutlet UILabel *menuNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *menuPriceLbl;
@property (weak, nonatomic) IBOutlet UIImageView *vegetarianImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cheffSpecialImageView;
@property (weak, nonatomic) IBOutlet UIImageView *spicyImageView;
@property (weak, nonatomic) IBOutlet UILabel *quantityLbl;
@property (weak, nonatomic) IBOutlet UITextView *foodDescriptionTextView;
- (IBAction)plusBtnPressed:(id)sender;
- (IBAction)minusBtnPressed:(id)sender;
@end
