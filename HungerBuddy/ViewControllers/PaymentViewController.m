//
//  ConfirmOrderViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/21/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "PaymentViewController.h"
#import "CurrentUser.h"
#import "Receipt.h"
#import "ConfirmOrderViewController.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (id)init
{
    self = [super initWithNibName:@"PaymentViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        date = [NSDate date];
        
        days = [[[NSDateFormatter alloc] init] weekdaySymbols];
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [gregorian setLocale:[NSLocale currentLocale]];
        
        NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
        
        currentWeek = nowComponents.week;
        currentWeekday = nowComponents.weekday;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self fillData];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kConfirmButton];
}

#pragma mark - Actions

- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (IBAction)dayBtnPressed:(id)sender {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSWeekdayCalendarUnit) fromDate:date];
    NSInteger day = [components weekday];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Days" rows:days initialSelection:(day - 1) doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        [gregorian setLocale:[NSLocale currentLocale]];
        [gregorian setTimeZone:[NSTimeZone defaultTimeZone]];
        
        NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
        
        if (currentWeekday > selectedIndex) {
            [nowComponents setWeek:currentWeek + 1];
        } else {
            [nowComponents setWeek:currentWeek];
        }
        
        [nowComponents setWeekday:selectedIndex + 1];
        
        date = [gregorian dateFromComponents:nowComponents];
        
        [self refreshSpinners];
        
    } cancelBlock:nil origin:sender];
}

- (IBAction)timeBtnPressed:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@"Time" datePickerMode:UIDatePickerModeTime selectedDate:date target:self action:@selector(timeSelected:element:) origin:sender];
}

- (IBAction)creditBtnPressed:(id)sender {
    ConfirmOrderViewController *confirmOrderVC = [[ConfirmOrderViewController alloc] initWithDate:date];
    
    UINavigationController *navigationVC = [[UINavigationController alloc] initWithRootViewController:confirmOrderVC];
    navigationVC.navigationBar.translucent = NO;
    [self presentViewController:navigationVC animated:YES completion:nil];
}

- (IBAction)debitBtnPressed:(id)sender {
#warning Implementation Missing
    [Utility showAlert:@"No implementation exists."];
}

- (IBAction)cashBtnPressed:(id)sender {
#warning Implementation Missing
    [Utility showAlert:@"No implementation exists."];
}

#pragma mark - Own Methods
- (void)fillData {
//    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    Receipt *receipt = [CurrentUser getCurrentReceipt];
    
    self.userAddressLbl.text = [CurrentUser getDeliveryAddressString];
    self.totalItemsLbl.text = [NSString stringWithFormat:@"%lu", (unsigned long)receipt.orders.count];
    self.totalAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", [receipt getTotalAmount]];
    
    [self refreshSpinners];
}

-(void) refreshSpinners {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"EEE";
    
    [self.dayBtn setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
    
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    [self.timeBtn setTitle:[dateFormatter stringFromDate:date] forState:UIControlStateNormal];
}

- (void)timeSelected:(NSDate *)selectedTime element:(id)sender
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:selectedTime];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [gregorian setLocale:[NSLocale currentLocale]];
    [gregorian setTimeZone:[NSTimeZone defaultTimeZone]];
    
    NSDateComponents *nowComponents = [gregorian components:NSYearCalendarUnit | NSWeekCalendarUnit | NSWeekdayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:date];
    
    [nowComponents setHour:hour];
    [nowComponents setMinute:minute];
    
    date = [gregorian dateFromComponents:nowComponents];
    
    [self refreshSpinners];
}

@end
