//
//  CommentsViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 5/21/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SocialFeedItem;

@interface CommentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (id)initWithSocialFeedItem:(SocialFeedItem *)aFeedItem;

@end
