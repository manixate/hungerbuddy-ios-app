//
//  RestaurantDetailViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Restaurant.h"
#import "SectionHeaderView.h"
#import "ModalView.h"
#import "FoodOptionView.h"

@protocol RestaurantDetailVCProtocol <NSObject>

- (void)incrementQuantity:(NSIndexPath *)indexPath;
- (void)decrementQuantity:(NSIndexPath *)indexPath;

@end

@interface RestaurantDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RestaurantDetailVCProtocol, SectionHeaderViewDelegate, ModalViewDelegate, FoodOptionDelegate>
{
    Restaurant *restaurant;
    NSArray *foodCategories;
    
    ModalView *modalView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andRestaurant:(Restaurant *) aRestaurant;

@property (nonatomic) NSMutableArray *sectionInfoArray;
@property (nonatomic) NSInteger openSectionIndex;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIView *topTotalAmountView;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalItemsLbl;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName;
@property (weak, nonatomic) IBOutlet UILabel *restaurantAddress;
@property (weak, nonatomic) IBOutlet UIButton *restaurantFollowStatusBtn;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *openLbl;
@end
