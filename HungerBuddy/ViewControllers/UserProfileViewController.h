//
//  UserProfileViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModalView.h"
#import "AddressEditView.h"

@interface UserProfileViewController : UIViewController <UITextFieldDelegate, ModalViewDelegate, AddressEditViewProtocol>
{
    UIActivityIndicatorView *userImageLoadingIndicator;
    UIActivityIndicatorView *followingCountLoadingIndicator;
    UIActivityIndicatorView *followersCountLoadingIndicator;
    UIActivityIndicatorView *favoritesCountLoadingIndicator;
    
    AddressEditView *addressEditView;
    ModalView *modalView;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *favoritesCountLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *addAddressBtn;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)rightBtnPressed:(id)sender;

- (IBAction)searchBtnPressed:(id)sender;
- (IBAction)editAddressBtnPressed:(id)sender;
- (IBAction)addAddressBtnPressed:(id)sender;
- (IBAction)followersBtnPressed:(id)sender;
- (IBAction)recentOrdersBtnPressed:(id)sender;
- (IBAction)foodBtnPressed:(id)sender;
- (IBAction)socialBtnPressed:(id)sender;
- (IBAction)dealsBtnPressed:(id)sender;
- (IBAction)logoutBtnPressed:(id)sender;
- (IBAction)orderNowBtn:(id)sender;
@end
