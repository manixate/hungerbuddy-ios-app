//
//  AddressEditView.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 19/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "AddressEditView.h"
#import "AFNetworking.h"
#import "ActivityIndicator.h"
#import "CurrentUser.h"

@implementation AddressEditView

- (id)init {
    self = [[[NSBundle mainBundle] loadNibNamed:@"AddressEditView" owner:self options:nil] firstObject];
    if (self) {
        self.nameTextField.text = [CurrentUser getDeliveryAddressName];
        self.addressTextField.text = [CurrentUser getDeliveryAddressAddress];
        self.cityTextField.text = [CurrentUser getDeliveryAddressCity];
        self.postalCodeTextField.text = [CurrentUser getDeliveryAddressPostalCode];
    }
    
    return self;
}

#pragma mark - Actions

- (IBAction)closeBtnPressed:(id)sender {
    [self.delegate close];
}

- (IBAction)saveBtnPressed:(id)sender {
    NSString *name = self.nameTextField.text;
    NSString *address = self.addressTextField.text;
    NSString *city = self.cityTextField.text;
    NSString *postalCode = self.postalCodeTextField.text;

    NSDictionary *addressDictionary = @{ADDRESS_NAME: name, ADDRESS_ADDR: address, ADDRESS_CITY: city, ADDRESS_POSTAL_CODE: postalCode};
    
    if (self.saveAsDefaultCheckbox.selected) {
        
        NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
        [standardDefaults setObject:addressDictionary forKey:DEFAULT_ADDRESS];
        [standardDefaults synchronize];
        
        NSLog(@"Saved default address: %@", addressDictionary);
    }
    
    [self.delegate saveWithAddressDictionary:addressDictionary];
}

- (IBAction)saveAsDefaultBtnPressed:(UIButton *)sender {
    sender.selected = !sender.selected;
}

@end
