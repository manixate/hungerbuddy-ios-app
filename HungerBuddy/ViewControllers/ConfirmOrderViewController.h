//
//  ConfirmOrderViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 30/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TPKeyboardAvoidingScrollView.h"

@interface ConfirmOrderViewController : UIViewController {
    NSDate *date;
    
    NSArray *splitUsersArr;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *itemLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UITextField *creditCardNumTextField;
@property (weak, nonatomic) IBOutlet UITextField *expiryMonthTextField;
@property (weak, nonatomic) IBOutlet UITextField *expiryYearTextField;
@property (weak, nonatomic) IBOutlet UIImageView *sliderImageView;

- (id)initWithDate:(NSDate *)aDate;

- (IBAction)splitBtnPressed:(id)sender;

@end
