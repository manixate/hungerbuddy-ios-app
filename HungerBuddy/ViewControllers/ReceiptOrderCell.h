//
//  ReceiptOrderCell.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/20/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReceiptOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *receiptOrderName;
@property (weak, nonatomic) IBOutlet UILabel *receiptOrderQuantity;
@property (weak, nonatomic) IBOutlet UILabel *receiptOrderAmount;
@end
