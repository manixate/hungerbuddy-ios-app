//
//  ReceiptOrderCell.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/20/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ReceiptOrderCell.h"

@implementation ReceiptOrderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
