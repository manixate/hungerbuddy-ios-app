//
//  FoodOptionCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 25/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodOptionCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ringImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemTextLbl;

@end
