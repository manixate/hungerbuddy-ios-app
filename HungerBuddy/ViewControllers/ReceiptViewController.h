//
//  ReceiptViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/15/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReceiptVCProtocol <NSObject>

- (void)incrementQuantity:(int)index;
- (void)decrementQuantity:(int)index;

@end

@class Restaurant;

@interface ReceiptViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ReceiptVCProtocol> {
    NSInteger currentEditingIdx;
    NSString *tempText;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UITableView *receiptOrderTableView;
@property (weak, nonatomic) IBOutlet UILabel *subTotal;
@property (weak, nonatomic) IBOutlet UILabel *taxTotal;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLbl;
@property (weak, nonatomic) IBOutlet UITextField *tipTextField;

- (IBAction)editAddressBtnPressed:(id)sender;
- (IBAction)roundSwitchValueChanged:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)confirmBtnPressed:(id)sender;
@end
