//
//  RestaurantCategoriesViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "RestaurantCategoriesViewController.h"
#import "Constants.h"
#import "AFHTTPRequestOperationManager.h"
#import "RestaurantCategory.h"
#import "Restaurant.h"
#import "RestaurantDetailViewController.h"
#import "CurrentUser.h"
#import "CategorySectionInfo.h"
#import "SectionHeaderView.h"
#import "ActivityIndicator.h"
#import "UIKit+AFNetworking.h"
#import "AFNetworking.h"
#import "RestaurantCell.h"

#define HEADER_HEIGHT 48

static NSString *SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";
static NSString *RestaurantCellIdentifier = @"RestaurantCellIdentifier";

@interface RestaurantCategoriesViewController ()
@end

@implementation RestaurantCategoriesViewController

- (id)init
{
    self = [super initWithNibName:@"RestaurantCategoriesViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        restaurantCategories = [[NSArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT;
    self.openSectionIndex = NSNotFound;
    
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RestaurantCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:RestaurantCellIdentifier];
    
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"RestaurantCell"
                                                                                    bundle:[NSBundle mainBundle]]
                                              forCellReuseIdentifier:RestaurantCellIdentifier];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kConfirmButton];
    
    [self loadData:[[CurrentUser getUserInfo] userID]];
    
    // iOS 7 specific
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (void)reloadData {
    if ((self.sectionInfoArray == nil) ||
        ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.tableView])) {
        
        // For each restaurant, set up a corresponding SectionInfo object to contain the default height for each row.
		NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
		for (RestaurantCategory *restaurantCategory in restaurantCategories) {
            
			CategorySectionInfo *sectionInfo = [[CategorySectionInfo alloc] init];
			sectionInfo.category = restaurantCategory;
			sectionInfo.open = NO;
            
            NSNumber *defaultRowHeight = @(RESTAURANT_ROW_HEIGHT);
			NSInteger countOfQuotations = [[(RestaurantCategory *)sectionInfo.category restaurant] count];
			for (NSInteger i = 0; i < countOfQuotations; i++) {
				[sectionInfo insertObject:defaultRowHeight inRowHeightsAtIndex:i];
			}
            
			[infoArray addObject:sectionInfo];
		}
        
		self.sectionInfoArray = infoArray;
	}
    
    [self.tableView reloadData];
}

#pragma mark - Load data
- (void)loadData:(int)userID {
    NSString *categoriesURL = [NSString stringWithFormat:@"%@%@", URL_BASE, CATEGORIES_URL];
    NSLog(@"Categories URL: %@", categoriesURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    AFHTTPRequestOperation *request;
    request = [manager GET:categoriesURL
                parameters:nil
                   success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request.URL);
             
             [self reloadData];
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSMutableArray *operations = [[NSMutableArray alloc] init];
         
         NSError *error;
         NSArray *categories = [MTLJSONAdapter modelsOfClass:[RestaurantCategory class] fromJSONArray:json error:&error];
         if (error)
         {
             NSLog(@"Got Error: %@", error);
             
             return;
         }
         
         for (RestaurantCategory *category in categories)
         {
             [operations addObject:[self getRestaurants:category]];
         }
         
         NSArray *connections = [AFHTTPRequestOperation batchOfRequestOperations:operations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
         } completionBlock:^(NSArray *operations) {
             restaurantCategories = [NSArray arrayWithArray:categories];
             searchResults = [NSArray arrayWithArray:categories];
             
             NSMutableArray *imageOperations = [[NSMutableArray alloc] init];
             
             for (RestaurantCategory *c in restaurantCategories) {
                 for (Restaurant *r in c.restaurant) {
                     NSURLRequest *request = [NSURLRequest requestWithURL:r.logoURL];
                     
                     AFHTTPRequestOperation *imageOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
                     imageOperation.responseSerializer = [AFImageResponseSerializer serializer];
                     [imageOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
                         r.logo = responseObject;
                     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                         r.logo = nil;
                     }];
                     
                     [imageOperations addObject:imageOperation];
                 }
             }
             
             NSArray *tempConnections = [AFHTTPRequestOperation batchOfRequestOperations:imageOperations progressBlock:nil completionBlock:^(NSArray *operations) {
                 
                 [self reloadData];
                 [indicator stopAnimating];
                 [self.navigationItem enableNavigationItem];
                 
             }];
             
             [[NSOperationQueue mainQueue] addOperations:tempConnections waitUntilFinished:NO];
         }];
         
         [[NSOperationQueue mainQueue] addOperations:connections waitUntilFinished:NO];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

- (AFHTTPRequestOperation *)getRestaurants:(RestaurantCategory *)category
{
    NSString *restaurantsURL = [NSString stringWithFormat:@"%@%@%d", URL_BASE, RES_CAT_ULR, category.ID];
    NSLog(@"Restaurants URL: %@", restaurantsURL);

    NSURLRequest *restaurantListRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:restaurantsURL]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:restaurantListRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
        
        if (!responseObject)
        {
            NSLog(@"Response NULL for getRestaurantts request: %@", operation.request.URL);
            
            category.restaurant = @[];
            
            return;
        }
        
        NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        category.restaurant = [MTLJSONAdapter modelsOfClass:Restaurant.class fromJSONArray:json error:&error];
        if (error)
        {
            NSLog(@"Got Error: %@", error);
            
            return;
        }
    
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        [Utility showAlert:@"Could not fetch data"];
    }];
    
    return operation;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return searchResults.count;
    }
    return self.sectionInfoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[indexPath.section];
    return [[sectionInfo objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [[searchResults objectAtIndex:section] restaurant].count;
    }
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
	NSInteger numStoriesInSection = [(RestaurantCategory *)sectionInfo.category restaurant].count;
    
    return sectionInfo.open ? numStoriesInSection : 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    SectionHeaderView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        UITableViewHeaderFooterView *headerView = [[UITableViewHeaderFooterView alloc] init];
        RestaurantCategory *category = searchResults[section];
        
        headerView.textLabel.text = category.categoryName;
        
        return headerView;
    }
    
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    sectionInfo.headerView = sectionHeaderView;
    
    sectionHeaderView.titleLabel.text = [sectionInfo.category categoryName];
    sectionHeaderView.itemCountLbl.text = [NSString stringWithFormat:@"%02d", [[(RestaurantCategory *)sectionInfo.category restaurant] count]];
    sectionHeaderView.section = section;
    sectionHeaderView.delegate = self;
    
    return sectionHeaderView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier:RestaurantCellIdentifier];
    if (cell == nil) {
        cell = [[RestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RestaurantCellIdentifier];
    }
    
    RestaurantCategory *restaurantCategory;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        restaurantCategory = [searchResults objectAtIndex:indexPath.section];
    } else {
        restaurantCategory = (RestaurantCategory *)[[self.sectionInfoArray objectAtIndex:indexPath.section] category];
//        restaurantCategory = [restaurantCategories objectAtIndex:indexPath.section];
    }
    
    Restaurant *restaurant = [restaurantCategory.restaurant objectAtIndex:indexPath.row];

//    cell.logoImageView.image = restaurant.logo;
    
    [cell.logoImageView setImageWithURL:restaurant.logoURL];
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greybar.PNG"]];
    cell.restaurantTitleLbl.text = restaurant.restaurantName;
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - SectionHeaderViewDelegate

- (void)sectionHeaderView:(SectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
	CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionOpened];
    
	sectionInfo.open = YES;
    
    /*
     Create an array containing the index paths of the rows to insert: These correspond to the rows for each quotation in the current section.
     */
    NSInteger countOfRowsToInsert = [[(RestaurantCategory *)sectionInfo.category restaurant] count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
        
        CategorySectionInfo *previousOpenSection = (self.sectionInfoArray)[previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        NSInteger countOfRowsToDelete = [[(RestaurantCategory *)previousOpenSection.category restaurant] count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    // style the animation so that there's a smooth flow in either direction
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // apply the updates
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    
    self.openSectionIndex = sectionOpened;
}

- (void)sectionHeaderView:(SectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionClosed];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
}

#pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    
     RestaurantCategory *restaurantCategory = [restaurantCategories objectAtIndex:indexPath.section];
     Restaurant *restaurant = [restaurantCategory.restaurant objectAtIndex:indexPath.row];
     RestaurantDetailViewController *detailViewController = [[RestaurantDetailViewController alloc] initWithNibName:@"RestaurantDetailViewController" bundle:nil andRestaurant:restaurant];
 
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // Pass the selected object to the new view controller.
 
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - SearchDisplayController methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//        RestaurantCategory *restaurantCategory = evaluatedObject;
//        
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.restaurantName contains[cd] %@", searchText];
//        NSArray *arr = [restaurantCategory.restaurant filteredArrayUsingPredicate:predicate];
//        return arr.count > 0;
//    }];
    
    NSMutableArray *categories = [[NSMutableArray alloc] init];
    
    for (RestaurantCategory *category in restaurantCategories) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.restaurantName contains[cd] %@", searchText];
        NSArray *arr = [category.restaurant filteredArrayUsingPredicate:predicate];
        
        if (arr.count > 0) {
            RestaurantCategory *c = [[RestaurantCategory alloc] initWithRestaurantCategory:category];
            c.restaurant = arr;
            
            [categories addObject:c];
        }
    }
    
    searchResults = [NSArray arrayWithArray:categories];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
