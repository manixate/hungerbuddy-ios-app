//
//  FoodOptionCell.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 25/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FoodOptionCell.h"

@implementation FoodOptionCell

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.transform = CGAffineTransformMakeRotation(M_PI_2);
        self.ringImageView.frame = CGRectMake(15, 0, 30, 30);
        
        UIView *selectedView = [[UIView alloc] init];
        selectedView.backgroundColor = [UIColor clearColor];
        self.selectedBackgroundView =  selectedView;
    }
    
    return self;
}

@end
