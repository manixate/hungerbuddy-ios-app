//
//  ConfirmOrderViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 30/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ConfirmOrderViewController.h"
#import "FinalReceiptViewController.h"
#import "ConfirmedOrder.h"
#import "CurrentUser.h"
#import "SplitUserCell.h"
#import "FinalSplitScreenViewController.h"
#import "SplitUserViewController.h"

static NSString *splitUserCellIdentifier = @"SplitUserCellIdentifier";

@interface ConfirmOrderViewController () <SplitUserViewControllerDelegate> {
    float totalAmount;
    BOOL splitEqually;
    
    NSInteger minutes;
}

@end

@implementation ConfirmOrderViewController

- (id)initWithDate:(NSDate *)aDate {
    self = [super initWithNibName:@"ConfirmOrderViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        date = aDate;
        
        User *currentUser = [CurrentUser getUserInfo];
        totalAmount = [[CurrentUser getCurrentReceipt] getTotalAmount];
        
        SplitData *splitData = [[SplitData alloc] init];
        splitData.name = currentUser.userName;
        splitData.email = currentUser.userEmail;
        splitData.amount = totalAmount;
        
        splitUsersArr = @[splitData];
        minutes = 0;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizer:)];
    [self.sliderImageView addGestureRecognizer:panGesture];
    self.sliderImageView.userInteractionEnabled = YES;
    
    [self configureNavBarWithLeftButton:kCrossButton titleButton:kButtonTypeNone andRightButton:kButtonTypeNone];
    [self fillData];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (void)splitBtnPressed:(id)sender {
    SplitUserViewController *splitUserViewController = [[SplitUserViewController alloc] initWithSplitArray:splitUsersArr totalAmount:totalAmount andMinutes:minutes];
    splitUserViewController.delegate = self;
    [self.navigationController pushViewController:splitUserViewController animated:YES];
}

#pragma mark - SplitUserViewControllerDelegate Methods
- (void)splitUsersListConfirmed:(NSArray *)splitArray andMinutes:(NSInteger)aMinutes {
    splitUsersArr = splitArray;
    minutes = aMinutes;
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Gesture Recognizer
- (void)panGestureRecognizer:(UIPanGestureRecognizer *)gesture {
    static CGPoint originalCenter;
    
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        originalCenter = gesture.view.center;
        gesture.view.layer.shouldRasterize = YES;
    }
    if (gesture.state == UIGestureRecognizerStateChanged)
    {
        CGPoint translate = [gesture translationInView:gesture.view.superview];
        gesture.view.center = CGPointMake(originalCenter.x + translate.x, gesture.view.center.y);
        
        float point = gesture.view.center.x;
        if (point >= 256) {
            [gesture.view removeGestureRecognizer:gesture];
            [self nextView];
            
            return;
        }
    }
    if (gesture.state == UIGestureRecognizerStateEnded ||
        gesture.state == UIGestureRecognizerStateFailed ||
        gesture.state == UIGestureRecognizerStateCancelled)
    {
        float point = gesture.view.frame.origin.x;
        if (point >= 256) {
            [gesture.view removeGestureRecognizer:gesture];
            [self nextView];
            
            return;
        }
        
        gesture.view.layer.shouldRasterize = NO;
        float y = gesture.view.center.y;
        gesture.view.userInteractionEnabled = false;
        
        [UIView animateWithDuration:0.2 animations:^{
            CGPoint center = CGPointMake(54, y);
            gesture.view.center = center;
        } completion:^(BOOL finished) {
            gesture.view.userInteractionEnabled = true;
        }];
    }
}

#pragma mark - Own Methods

- (void)nextView {
    ConfirmedOrder *confirmedOrder = [[ConfirmedOrder alloc] init];
    confirmedOrder.receipt = [CurrentUser getCurrentReceipt];
    confirmedOrder.deliveryTime = [date dateByAddingTimeInterval:minutes * 60];
    confirmedOrder.deliveryAddress = [CurrentUser getDeliveryAddressString];
    confirmedOrder.splitUsers = splitUsersArr;
    
    if (confirmedOrder.splitUsers.count <= 1) {
        FinalReceiptViewController *finalReceiptVC = [[FinalReceiptViewController alloc] initWithConfirmedOrder:confirmedOrder];
        [self.navigationController pushViewController:finalReceiptVC animated:YES];
    } else {
        FinalSplitScreenViewController *finalReceiptVC = [[FinalSplitScreenViewController alloc] initWithConfirmedOrder:confirmedOrder];
        [self.navigationController pushViewController:finalReceiptVC animated:YES];
    }
}

- (void)fillData
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    dateFormatter.timeStyle = NSDateFormatterNoStyle;
    
    self.dateLbl.text = [dateFormatter stringFromDate:date];
    
    dateFormatter.dateStyle = NSDateFormatterNoStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    self.timeLbl.text = [dateFormatter stringFromDate:date];
    
//    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    Receipt *receipt = [CurrentUser getCurrentReceipt];
    
    self.addressLbl.text = [CurrentUser getDeliveryAddressString];
    self.itemLbl.text = [NSString stringWithFormat:@"%lu", (unsigned long)receipt.orders.count];
    self.totalPriceLbl.text = [NSString stringWithFormat:@"$ %0.2f", [receipt getTotalAmount]];
}

@end
