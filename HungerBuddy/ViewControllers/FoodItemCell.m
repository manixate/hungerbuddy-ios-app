//
//  FoodItemCell.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 23/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FoodItemCell.h"

@implementation FoodItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)plusBtnPressed:(id)sender {
    [self.delegate incrementQuantity:self.indexPath];
}

- (IBAction)minusBtnPressed:(id)sender {
    [self.delegate decrementQuantity:self.indexPath];
}
@end
