//
//  FollowersViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "FollowersViewController.h"
#import "SocialCell.h"
#import "User.h"
#import "AFNetworking.h"
#import "CurrentUser.h"
#import "ActivityIndicator.h"
#import "SocialViewController.h"
#import "UIKit+AFNetworking.h"
#import "FollowUser.h"

@interface FollowersViewController ()
{
    NSArray *searchResults;
}

@end

@implementation FollowersViewController

@synthesize socialList, segmentControl, tableView = _tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        socialList = @[];
        searchResults = @[];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"SocialCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"SocialCellIdentifier"];
    self.tableView.rowHeight = FOLLOWER_ROW_HEIGHT;

    segmentControl = [[SVSegmentedControl alloc] initWithSectionTitles:@[@"Loved", @"Following", @"Followers"]];
    segmentControl.thumb.tintColor = [UIColor colorWithRed:255/255.0 green:127/255.0 blue:0/255.0 alpha:1.0];
    segmentControl.thumb.textShadowColor = [UIColor clearColor];
    segmentControl.backgroundTintColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    segmentControl.textShadowColor = [UIColor clearColor];
    
    segmentControl.frame = self.segmentControlView.bounds;
    [segmentControl addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
    
    [self.segmentControlView addSubview:segmentControl];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kButtonTypeNone];
    
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"SocialCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"SocialCellIdentifier"];
    
    // iOS 7 specific
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (IBAction)segmentChanged:(SVSegmentedControl *)sender {
    User *user = [CurrentUser getUserInfo];
    
    switch (segmentControl.selectedSegmentIndex)
    {
        case 0:
            socialList = @[];
            [self.tableView reloadData];
            break;
        case 1:
            [self fetchFollowersList:user.userID followerType:FOLLOWING];
            break;
        case 2:
            [self fetchFollowersList:user.userID followerType:FOLLOWERS];
            break;
            
        default:
            break;
    }
}

#pragma mark - Own Methods
- (void)fetchFollowersList:(int)userID followerType:(NSString *)followerType
{
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    NSString *urlString = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, GET_FOLLOWERSLIST], followerType, userID];
    
    socialList = @[];
    [self.tableView reloadData];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:urlString
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             
             [self.tableView reloadData];
             return;
         }
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSError *error;
         socialList = [MTLJSONAdapter modelsOfClass:FollowUser.class fromJSONArray:json error:&error];
         if (error)
             NSLog(@"Error while converting to FollowUser: %@", error);
                  
         [self.tableView reloadData];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView)
        return searchResults.count;
    else
        return socialList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SocialCellIdentifier";
    SocialCell *cell = (SocialCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (SocialCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    FollowUser *user;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
        user = searchResults[indexPath.row];
    else
        user = socialList[indexPath.row];
    
    [cell.userImageView setImageWithURL:user.user.userImageURL];
    cell.userName.text = user.user.userName;
    cell.userAddress.text = @"";
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

 #pragma mark - Table view delegate
 
 // In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
 {
     FollowUser *followUser;
     if (tableView == self.searchDisplayController.searchResultsTableView)
         followUser = searchResults[indexPath.row];
     else
         followUser = socialList[indexPath.row];
     
     User *user = followUser.user;
     SocialViewController *socialVC = [[SocialViewController alloc] initWithUser:user];
     [self.navigationController pushViewController:socialVC animated:YES];
 }

#pragma mark - SearchDisplayController methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user.userName contains[cd] %@", searchText];
    searchResults = [socialList filteredArrayUsingPredicate:predicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark - SocialCellDelegate methods
- (void)socialCellButtonPressed:(int)index
{
    User *user = socialList[index];
    int segmentIndex = segmentControl.selectedSegmentIndex;
    
}

@end
