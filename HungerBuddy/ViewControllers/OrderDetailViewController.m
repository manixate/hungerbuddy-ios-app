//
//  OrderDetailViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 6/9/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "OrderDetailViewController.h"
#import "OrderDetailCell.h"
#import "OrderDetail.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"

static NSString *cellIdentifier = @"OrderDetailCellIdentifier";

@interface OrderDetailViewController () <UITableViewDataSource, UITableViewDelegate>
{
    int orderID;
    NSArray *orderDetails;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation OrderDetailViewController

- (id)initWithOrderID:(int)anOrderID
{
    self = [super initWithNibName:@"OrderDetailViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        orderID = anOrderID;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderDetailCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
    
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods

- (void)loadData
{
    NSString *orderDetailURL = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, ORDER_DETAIL_URL], orderID, @"Dummy"];
    
    NSLog(@"Order Detail URL: %@", orderDetailURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:orderDetailURL
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSMutableArray *tempArr = [[NSMutableArray alloc] init];
         
         for (NSDictionary *c in json) {
             OrderDetail *detail = [[OrderDetail alloc] init];
             
             detail.email = [c objectForKey:@"email"];
             detail.amount = [[c objectForKey:@"bill_amount"] doubleValue];
             detail.isVerified = [[c objectForKey:@"cc_verified"] boolValue];
             detail.isProcessed = [[c objectForKey:@"cc_processed"] boolValue];
             detail.orderToken = [c objectForKey:@"order_monitor_token"];
             
             [tempArr addObject:detail];
         }
         
         orderDetails = tempArr;
         [self.tableView reloadData];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return orderDetails.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[OrderDetailCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    OrderDetail *detail = orderDetails[indexPath.row];
    
    cell.emailLbl.text = detail.email;
    cell.isVerifiedLbl.text = detail.isVerified ? @"Yes" : @"No";
    cell.isProcessedLbl.text = detail.isProcessed ? @"Yes" : @"No";
    cell.amountLbl.text = [NSString stringWithFormat:@"$ %.02f", detail.amount];
    
    return cell;
}


@end
