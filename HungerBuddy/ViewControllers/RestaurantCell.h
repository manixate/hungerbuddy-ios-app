//
//  RestaurantCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 26/01/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *restaurantTitleLbl;

@end
