//
//  RecentOrderViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "RecentOrderViewController.h"
#import "OrderCell.h"
#import "AFNetworking.h"
#import "CurrentUser.h"
#import "Order.h"
#import "ActivityIndicator.h"
#import "OrderDetailViewController.h"

@interface RecentOrderViewController ()

@end

@implementation RecentOrderViewController

@synthesize orders;

- (id)init
{
    self = [super initWithNibName:@"RecentOrderViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        orders = [NSMutableArray array];
        searchResults = @[];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"OrderCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:@"OrderCellIdentifier"];
    
    [self.searchDisplayController.searchResultsTableView registerNib:[UINib nibWithNibName:@"OrderCell"
                                                                                    bundle:[NSBundle mainBundle]]
                                              forCellReuseIdentifier:@"OrderCellIdentifier"];
    
    [self loadData:[[CurrentUser getUserInfo] userID]];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kButtonTypeNone];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

#pragma mark - Load data
- (void)loadData:(int)userID {
    NSString *recentURL = [NSString stringWithFormat:@"%@%@%d", URL_BASE, USER_RECENT_ORDERS, userID];
    NSLog(@"Recent URL: %@", recentURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:recentURL
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         
         //sesl87aedrqvec7rdhlnuaisv3
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             
             searchResults = @[];
             [self.tableView reloadData];
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         [orders removeAllObjects];
         
         for (NSDictionary *restaurant in json) {
             for (NSDictionary *o in [restaurant objectForKey:@"order_details"]) {
                 Order *order = [[Order alloc] init];
                 
                 order.ID = [[o objectForKey:@"id"] intValue];
                 order.orderID = [[o objectForKey:@"order_id"] intValue];
                 order.menuID = [[o objectForKey:@"menu_id"] intValue];
                 order.menuName = [o objectForKey:@"menu_name"];
                 order.menuSizeID = [[o objectForKey:@"menu_size_id"] intValue];
                 order.menuSize = [o objectForKey:@"menu_size"];
                 order.menuMeatID = [[o objectForKey:@"menu_meat_id"] intValue];
                 order.menuMeat = [o objectForKey:@"menu_meat"];
                 order.sizePrice = [[o objectForKey:@"size_price"] floatValue];
                 order.meatPrice = [[o objectForKey:@"meat_price"] floatValue];
                 order.quantity = [[o objectForKey:@"qty"] intValue];
                 order.spicy = [o objectForKey:@"spicy"];
                 order.totalPrice = [[o objectForKey:@"total_price"] floatValue];
                 
                 [orders addObject:order];
             }
         }
         
         searchResults = [NSArray arrayWithArray:orders];
         [self.tableView reloadData];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return RECIPE_ROW_HEIGHT;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return [searchResults count];
    }
    return orders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"OrderCellIdentifier";
    OrderCell *cell = (OrderCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = (OrderCell *)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    Order *order;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        order = [searchResults objectAtIndex:indexPath.row];
    } else {
        order = [orders objectAtIndex:indexPath.row];
    }
    
    cell.orderNameLbl.text = order.menuName;
    cell.orderPriceLbl.text = [NSString stringWithFormat: @"%0.3f", order.totalPrice];
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Order *order = orders[indexPath.row];

    OrderDetailViewController *detailViewController = [[OrderDetailViewController alloc] initWithOrderID:order.orderID];

    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma mark - SearchDisplayController methods
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"SELF.menuName contains[cd] %@",
                                    searchText];
    
    searchResults = [orders filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
