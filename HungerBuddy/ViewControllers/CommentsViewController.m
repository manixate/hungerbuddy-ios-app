//
//  CommentsViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 5/21/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "CommentsViewController.h"
#import "SocialFeedItem.h"
#import "CommentCell.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"

static NSString *cellIdentifier = @"CommentCellIdentifier";

@interface CommentsViewController ()
{
    SocialFeedItem *feedItem;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
- (IBAction)commentBtnPressed:(id)sender;
@end

@implementation CommentsViewController

- (id)initWithSocialFeedItem:(SocialFeedItem *)aFeedItem
{
    self = [super initWithNibName:@"CommentsViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        feedItem = aFeedItem;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self.tableView registerNib:[UINib nibWithNibName:@"CommentCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:cellIdentifier];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return feedItem.comments.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Comment *comment = feedItem.comments[indexPath.row];
    CGSize size = [comment.commentDescription sizeWithFont:[UIFont systemFontOfSize:18] forWidth:tableView.frame.size.width lineBreakMode:NSLineBreakByWordWrapping];
    
    return MAX(95, size.height + 20);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    Comment *comment = feedItem.comments[indexPath.row];
    
    [cell.profileImageView setImageWithURL:comment.user.userImageURL];
    cell.commentDescription.text = comment.commentDescription;
    cell.commentUser.text = comment.user.userName;
    cell.commentDateAdded.text = comment.timeElapsed;
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Comment *comment = feedItem.comments[indexPath.row];
        [self deleteComment:comment.commentID];
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[DetailViewController alloc] initWithNibName:@"<#Nib name#>" bundle:nil];

    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
 
 */

#pragma mark - Private Methods
- (void)postComment:(NSString *)comment
{
    NSString *commentURLString = [NSString stringWithFormat:@"%@%@", URL_BASE, COMMENT_URL];
    NSDictionary *param = @{@"post_id": @(feedItem.postID), @"description": comment};
    
    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:commentURLString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)[operation.response statusCode]);
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            [activityIndicator stopAnimating];
            
            return;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        Comment *comment = [MTLJSONAdapter modelOfClass:Comment.class fromJSONDictionary:json error:&error];
        
        if (!error)
        {
            feedItem.comments = [feedItem.comments arrayByAddingObject:comment];
            
            [self.tableView reloadData];
        }
        
        [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        [Utility showAlert:@"Could not add comment"];
        
        [activityIndicator stopAnimating];
    }];
}

- (void)deleteComment:(int)commentID
{
    NSString *commentURLString = [NSString stringWithFormat:@"%@%@", URL_BASE, COMMENT_URL];
    NSDictionary *param = @{@"id": @(commentID)};
    
    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager DELETE:commentURLString parameters:param success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)[operation.response statusCode]);
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            [activityIndicator stopAnimating];
            
            return;
        }
        
        __block Comment *comment;
        [feedItem.comments enumerateObjectsUsingBlock:^(Comment *obj, NSUInteger idx, BOOL *stop) {
            if (obj.commentID == commentID)
            {
                comment = obj;
                
                stop = true;
            }
        }];
        
        if (comment)
        {
            NSMutableArray *temp = [feedItem.comments mutableCopy];
            [temp removeObject:comment];
            
            [self.tableView reloadData];
        }
        [activityIndicator stopAnimating];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        [Utility showAlert:@"Could not delete comment"];
        
        [activityIndicator stopAnimating];
    }];
}

#pragma mark - Actions
- (IBAction)commentBtnPressed:(id)sender {
    NSString *commentText = self.commentTextField.text;
    
    if (commentText.length > 0)
    {
        [self postComment:commentText];
    }
}
@end
