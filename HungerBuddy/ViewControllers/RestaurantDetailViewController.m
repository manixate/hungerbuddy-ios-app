//
//  RestaurantDetailViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/14/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "RestaurantDetailViewController.h"
#import "ReceiptViewController.h"
#import "AFHTTPRequestOperationManager.h"
#import "FoodCategory.h"
#import "FoodItem.h"
#import "FoodItemCell.h"
#import "CurrentUser.h"
#import "SectionHeaderView.h"
#import "CategorySectionInfo.h"
#import "ActivityIndicator.h"
#import "LoginViewController.h"

#define HEADER_HEIGHT 48

static NSString *SectionHeaderViewIdentifier = @"SectionHeaderViewIdentifier";

@interface RestaurantDetailViewController () <LoginDelegate>

@end

@implementation RestaurantDetailViewController

@synthesize restaurantName, restaurantAddress, restaurantImage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andRestaurant:(Restaurant *) aRestaurant
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        restaurant = aRestaurant;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    restaurantName.text = restaurant.restaurantName;
    restaurantAddress.text = restaurant.address;
    
    self.tableView.sectionHeaderHeight = HEADER_HEIGHT;
    self.openSectionIndex = NSNotFound;
    
    UINib *sectionHeaderNib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.tableView registerNib:sectionHeaderNib forHeaderFooterViewReuseIdentifier:SectionHeaderViewIdentifier];
    
//    UIButton *controlPanel = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//    [controlPanel setBackgroundColor:[UIColor blackColor]];
//    [controlPanel addTarget:self action:@selector(controlPanel:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.navigationItem.titleView = self.topTotalAmountView;
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(confirmBtnPressed:)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"FoodItemCell" bundle:nil] forCellReuseIdentifier:@"FoodItemCellIdentifier"];
    
    [self fillData];
    

    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kButtonTypeNone andRightButton:kConfirmButton];
    self.navigationItem.titleView = self.topTotalAmountView;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadData];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
    User *userInfo = [CurrentUser getUserInfo];
    if ([userInfo.userEmail isEqualToString:@""]) {
        LoginViewController *loginVC = [[LoginViewController alloc] initWithNibName:@"LoginView" bundle:nil];
        loginVC.delegate = self;
        [self presentViewController:loginVC animated:YES completion:nil];
    } else {
        ReceiptViewController *receiptVC = [[ReceiptViewController alloc] initWithNibName:@"ReceiptViewController" bundle:nil];
        [self.navigationController pushViewController:receiptVC animated:YES];
    }
}
- (IBAction)restaurantFollowStatusBtnPressed:(id)sender {
    // If user is following the restaurant
    if (self.restaurantFollowStatusBtn.selected)
        [self followRestaurant:false];
    else
        [self followRestaurant:true];
}

#pragma mark - LoginDelegate Methods
-(void)userLoggedIn
{
    NSLog(@"User Loged In");
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)userLoginCancelled
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Own Methods
- (void)fillData {
    [self.restaurantImage setImageWithURL:restaurant.logoURL];
    self.restaurantFollowStatusBtn.selected = restaurant.isUserFollowing;
    
    if (restaurant.isOpen)
        self.openLbl.text = @"Open";
    else
        self.openLbl.text = @"Closed";
}

- (void)reloadData {
    
    if ((self.sectionInfoArray == nil) ||
        ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.tableView])) {
        
        // For each item, set up a corresponding SectionInfo object to contain the default height for each row.
		NSMutableArray *infoArray = [[NSMutableArray alloc] init];
        
		for (FoodCategory *foodCategory in foodCategories) {
            
			CategorySectionInfo *sectionInfo = [[CategorySectionInfo alloc] init];
			sectionInfo.category = foodCategory;
			sectionInfo.open = NO;
            
            NSNumber *defaultRowHeight = @(FOODITEM_ROW_HEIGHT);
			NSInteger countOfQuotations = [[(FoodCategory *)sectionInfo.category foodList] count];
			for (NSInteger i = 0; i < countOfQuotations; i++) {
				[sectionInfo insertObject:defaultRowHeight inRowHeightsAtIndex:i];
			}
            
			[infoArray addObject:sectionInfo];
		}
        
		self.sectionInfoArray = infoArray;
	}
    
    [self.tableView reloadData];
}

- (void)loadData {
    NSString *foodCategoriesURL = [NSString stringWithFormat:@"%@%@%d", URL_BASE, RES_FOOD_CAT_URL, restaurant.ID];
    NSLog(@"Food Categories URL: %@", foodCategoriesURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:foodCategoriesURL
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             [self updateTopTotalAmountView];
             
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSMutableArray *operations = [[NSMutableArray alloc] init];
         
         NSError *error;
         NSArray *categories = [MTLJSONAdapter modelsOfClass:[FoodCategory class] fromJSONArray:json error:&error];
         if (error)
         {
             NSLog(@"Got Error: %@", error);
             
             return;
         }
         
         for (FoodCategory *category in categories)
         {
             [operations addObject:[self getFoodList:category]];
         }
         
         NSArray *connections = [AFHTTPRequestOperation batchOfRequestOperations:operations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
         } completionBlock:^(NSArray *operations) {
             __block BOOL failed = false;
             [operations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                 AFHTTPRequestOperation *operation = obj;
                 
                 // We got error in one of our operations so show an alert.
                 if (operation.error)
                 {
                     failed = true;
                     *stop = true;
                 }
             }];
             
             if (failed)
             {
                 [Utility showAlert:@"Could not fetch complete data"];
             }
             
             foodCategories = [NSArray arrayWithArray:categories];
//             searchResults = [NSArray arrayWithArray:categories];
             [self reloadData];
             
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             
             [self updateTopTotalAmountView];
         }];
         
         [[NSOperationQueue mainQueue] addOperations:connections waitUntilFinished:NO];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}

- (AFHTTPRequestOperation *)getFoodList:(FoodCategory *)category
{
    NSString *foodListURL = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, FOOD_LIST_URL], category.ID, restaurant.ID];
    NSLog(@"Food List URL: %@", foodListURL);
    
    NSURLRequest *restaurantListRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:foodListURL]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:restaurantListRequest];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            category.foodList = @[];
            
            return;
        }
        
        NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        NSArray *foodItems = [MTLJSONAdapter modelsOfClass:[FoodItem class] fromJSONArray:json error:&error];
        if (error)
        {
            NSLog(@"Got error: %@", error);
            
            return;
        }
        
        category.foodList = foodItems;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
    }];
    
    return operation;
}

- (void) updateTopTotalAmountView {
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    
    Receipt *receipt = currentUser.currentReceipt;
    
    self.totalAmountLbl.text = [NSString stringWithFormat:@"%0.2f", [receipt getTotalAmount]];
    self.totalItemsLbl.text = [NSString stringWithFormat:@"%d", receipt.orders.count];
}

- (void) followRestaurant:(BOOL)follow {
    NSString *requestURLString;
    if (follow)
        requestURLString = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, RES_FOLLOW], restaurant.ID];
    else
        requestURLString = [NSString stringWithFormat:[NSString stringWithFormat:@"%@%@", URL_BASE, RES_UNFOLLOW], restaurant.ID];
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:requestURLString
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            [indicator stopAnimating];
            [self.navigationItem enableNavigationItem];
            [self updateTopTotalAmountView];
            
            return;
        }
        
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        NSString *message = [json objectForKey:@"message"];
//        if (message)
        {
            message = [NSString stringWithFormat:@"You %@followed %@", (follow ? @"" : @"un"), restaurant.restaurantName];
            
            self.restaurantFollowStatusBtn.selected = follow;
        }
        
        [Utility showAlert:message];
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        [Utility showAlert:@"Could not follow/unfollow restaurant"];
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    }];
}

#pragma mark - ModalViewDelegate methods
- (void)modalViewDidAppear:(ModalView *)aModalView {
    FoodOptionView *foodOptionView = (FoodOptionView *)modalView.modalView;
    [foodOptionView setSelections];
}

- (void)modalViewDidDisappear:(ModalView *)aModalView {

}

#pragma mark - FoodOptionDelegate methods
- (void)close:(FoodItem *)foodItem {
    [modalView hideModalView];
    modalView = nil;
    
//    [self.tableView reloadData];
    
    NSLog(@"Updated FoodItem: %@", foodItem);
    
    if (self.openSectionIndex == NSNotFound)
        return;
    
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[self.openSectionIndex];
    
    NSUInteger countOfRowsToUpdate = [[(FoodCategory *)sectionInfo.category foodList] count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToUpdate; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:self.openSectionIndex]];
    }
         
    [self.tableView reloadRowsAtIndexPaths:indexPathsToInsert withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    SectionHeaderView *sectionHeaderView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:SectionHeaderViewIdentifier];
    
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
    sectionInfo.headerView = sectionHeaderView;
    
    sectionHeaderView.titleLabel.text = [sectionInfo.category categoryName];
    sectionHeaderView.itemCountLbl.text = [NSString stringWithFormat:@"%02lu", [[(FoodCategory *)sectionInfo.category foodList] count]];
    sectionHeaderView.section = section;
    sectionHeaderView.delegate = self;
    
    return sectionHeaderView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return self.sectionInfoArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[indexPath.section];
    return [[sectionInfo objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[section];
	NSInteger numStoriesInSection = [(FoodCategory *)sectionInfo.category foodList].count;
    
    return sectionInfo.open ? numStoriesInSection : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"FoodItemCellIdentifier";
    FoodItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[FoodItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
  
    FoodCategory *foodCategory = (FoodCategory *)[[self.sectionInfoArray objectAtIndex:indexPath.section] category];
    FoodItem *item = [[foodCategory foodList] objectAtIndex:indexPath.row];
//    FoodItem *item = [[[foodCategories objectAtIndex:indexPath.section] foodList] objectAtIndex:indexPath.row];
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    
    Receipt *receipt = currentUser.currentReceipt;
    
    Order *order = [receipt getOrderFromFoodItem:item];
    if (order != nil) {
        cell.quantityLbl.text = [NSString stringWithFormat:@"%02d", order.quantity];
    } else {
        cell.quantityLbl.text = @"00";
    }
    
    cell.delegate = self;
    cell.indexPath = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
    
    cell.menuNameLbl.text = item.menuName;
    cell.menuPriceLbl.text = [NSString stringWithFormat:@"$ %0.2f", item.price];
    cell.foodDescriptionTextView.text = item.foodItemDescription;
    
    if ([item.vegetarian isEqualToString:@"1"])
        cell.vegetarianImageView.hidden = false;
    else
        cell.vegetarianImageView.hidden = true;
    
    if ([item.chefsSpecial isEqualToString:@"1"])
        cell.cheffSpecialImageView.hidden = false;
    else
        cell.cheffSpecialImageView.hidden = true;
    
    if ([item.spiceOption isEqualToString:@"1"])
        cell.spicyImageView.hidden = false;
    else
        cell.spicyImageView.hidden = true;
    
    return cell;
}

#pragma mark - Table view delegate
 
// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FoodCategory *foodCategory = [foodCategories objectAtIndex:indexPath.section];
    FoodItem *foodItem = [foodCategory.foodList objectAtIndex:indexPath.row];
    FoodOptionView *foodOptionView = [[FoodOptionView alloc] initWithFoodItem:foodItem];
    foodOptionView.delegate = self;
    
    modalView = [[ModalView alloc] initWithSuperView:self.view andModalView:foodOptionView];
    modalView.delegate = self;
    
    [modalView showModalView];
    
    NSLog(@"Clicked FoodItem: %@", foodItem);
}

#pragma mark - SectionHeaderViewDelegate

- (void)sectionHeaderView:(SectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    
	CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionOpened];
    
	sectionInfo.open = YES;
    
    /*
     Create an array containing the index paths of the rows to insert: These correspond to the rows for each quotation in the current section.
     */
    NSInteger countOfRowsToInsert = [[(FoodCategory *)sectionInfo.category foodList] count];
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = self.openSectionIndex;
    if (previousOpenSectionIndex != NSNotFound) {
        
        CategorySectionInfo *previousOpenSection = (self.sectionInfoArray)[previousOpenSectionIndex];
        previousOpenSection.open = NO;
        [previousOpenSection.headerView toggleOpenWithUserAction:NO];
        NSInteger countOfRowsToDelete = [[(FoodCategory *)previousOpenSection.category foodList] count];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
    }
    
    // style the animation so that there's a smooth flow in either direction
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // apply the updates
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    
    self.openSectionIndex = sectionOpened;
}

- (void)sectionHeaderView:(SectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    CategorySectionInfo *sectionInfo = (self.sectionInfoArray)[sectionClosed];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
}

#pragma mark - RestaurantDetailVCProtocol methods
- (void)incrementQuantity:(NSIndexPath *)indexPath
{
    FoodItem *item = [[[foodCategories objectAtIndex:indexPath.section] foodList] objectAtIndex:indexPath.row];
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    
    Receipt *receipt = currentUser.currentReceipt;
    
    [receipt updateOrderListWithFoodItem:item isIncrement:YES];
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self updateTopTotalAmountView];
}

- (void)decrementQuantity:(NSIndexPath *)indexPath
{
    FoodItem *item = [[[foodCategories objectAtIndex:indexPath.section] foodList] objectAtIndex:indexPath.row];
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    
    Receipt *receipt = currentUser.currentReceipt;
    
    [receipt updateOrderListWithFoodItem:item isIncrement:NO];
    
    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self updateTopTotalAmountView];
}

@end
