//
//  SocialFeedItemCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 16/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SocialFeedItemCellDelegate <NSObject>

- (void)commentsBtnPressed:(int)index;
- (void)shareBtnPressed:(int)index;

@end

@interface SocialFeedItemCell : UITableViewCell

@property (nonatomic) NSInteger index;
@property (weak, nonatomic) id<SocialFeedItemCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *feedTime;
@property (weak, nonatomic) IBOutlet UIImageView *feedImageView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *commentsCountLbl;

- (IBAction)commentsBtnPressed:(id)sender;
- (IBAction)shareBtnPressed:(id)sender;

@end
