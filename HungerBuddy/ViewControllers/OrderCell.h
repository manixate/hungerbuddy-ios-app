//
//  OrderCell.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/9/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *orderDate;
@property (weak, nonatomic) IBOutlet UILabel *orderDay;
@property (weak, nonatomic) IBOutlet UILabel *orderPriceLbl;
@end
