//
//  SocialCell.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "SocialCell.h"

@implementation SocialCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)userButtonPressed:(id)sender {
    [self.delegate socialCellButtonPressed:self.index];
}
@end
