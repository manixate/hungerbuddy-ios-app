//
//  ReceiptViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/15/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ReceiptViewController.h"
#import "CurrentUser.h"
#import "ReceiptOrderCell.h"
#import "ReceiptOrderEditCell.h"
#import "Order.h"
#import "PaymentViewController.h"
#import "AddressEditView.h"
#import "ModalView.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"
#import "CurrentUser.h"

static NSString *ReceiptOrderCellIdentifier = @"ReceiptOrderCellIdentifier";
static NSString *ReceiptOrderEditCellIdentifier = @"ReceiptOrderEditCellIdentifier";
static NSString *ReceiptsCellIdentifer = @"ReceiptsCellIdentifer";

@interface ReceiptViewController () <AddressEditViewProtocol, ModalViewDelegate> {
    AddressEditView *addressEditView;
    ModalView *modalView;
}

@end

@implementation ReceiptViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        currentEditingIdx = 0;
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    addressEditView = [[AddressEditView alloc] init];
    modalView = [[ModalView alloc] initWithSuperView:self.view andModalView:addressEditView];
    
    addressEditView.delegate = self;
    modalView.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.receiptOrderTableView registerNib:[UINib nibWithNibName:@"ReceiptOrderCell" bundle:nil] forCellReuseIdentifier:ReceiptOrderCellIdentifier];
    [self.receiptOrderTableView registerNib:[UINib nibWithNibName:@"ReceiptOrderEditCell" bundle:nil] forCellReuseIdentifier:ReceiptOrderEditCellIdentifier];
    
    self.receiptOrderTableView.backgroundColor = [UIColor clearColor];
    
    [self fillData];
    

    
    [self setupKeyboard];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kButtonTypeNone];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (IBAction)editAddressBtnPressed:(id)sender {
    [modalView showModalView];
}

- (IBAction)roundSwitchValueChanged:(id)sender {
#warning No call implemented
    NSLog(@"Round value: %@", ((UISwitch *)sender).isOn ? @"YES": @"NO");
}

- (IBAction)cancelBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmBtnPressed:(id)sender {
    PaymentViewController *confirmOrderVC = [[PaymentViewController alloc] init];
    [self.navigationController pushViewController:confirmOrderVC animated:YES];
}

- (IBAction)editingBegan:(UITextField *)sender {
    NSString *text = sender.text;
    
    text = [text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    sender.text = [NSString stringWithFormat:@"%@", text];
    
    tempText = text;
}

- (IBAction)editingEnded:(UITextField *)sender {
    NSString *text = sender.text;
    
    text = [text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    text = [text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    sender.text = [NSString stringWithFormat:@"$ %@", text];
}
#pragma mark - AddressEditViewDelegate Methods
- (void)close {
    [modalView hideModalView];
}

- (void)saveWithAddressDictionary:(NSDictionary *)addressDictionary {
//    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameters = @{@"name": name, @"address": address, @"city": city, @"postal_code": postalCode};
//    
//    [manager POST:EDIT_USER_INFO_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Got response for updating user info: %@", responseObject);
//        NSLog(@"Updating user info successfull");
//        
//        User *currentUser = [CurrentUser getUserInfo];
//        currentUser.userCity = city;
//        currentUser.userAddress = address;
//        currentUser.userPostalCode = postalCode;
//        
//        [self fillData];
//
    [CurrentUser getCurrentUser].deliveryAddressDictionary = addressDictionary;
        [modalView hideModalView];
//
//        [activityIndicator stopAnimating];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Got error response for updating user info: %@", error);
//        NSLog(@"Updating user info failed");
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error changing user data." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alertView show];
//        
//        [modalView hideModalView];
//        
//        [activityIndicator stopAnimating];
//    }];
}

#pragma mark - ModalViewDelegate Methods
- (void)modalViewDidAppear:(ModalView *)modalView {
    
}

- (void)modalViewDidDisappear:(ModalView *)modalView {
    
}

#pragma mark - Own Methods
- (void)setupKeyboard
{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    self.tipTextField.inputAccessoryView = numberToolbar;
}

-(void)cancelNumberPad{
    self.tipTextField.text = tempText;
    
    [self.tipTextField resignFirstResponder];
    
    tempText = @"";
}

-(void)doneWithNumberPad{
    NSString *amountText = self.tipTextField.text;
    
    amountText = [amountText stringByReplacingOccurrencesOfString:@"$" withString:@""];
    amountText = [amountText stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    float value = [amountText floatValue];
    
    [CurrentUser getCurrentReceipt].tip = value;
    
    [self updateAmounts];
    
    [self.tipTextField resignFirstResponder];
    
    tempText = @"";
}

- (void)fillData
{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    //    [currentUser fillTestData];
    
    [self.userImageView setImageWithURL:currentUser.user.userImageURL];
    self.userNameLbl.text = currentUser.user.userName;
    self.addressLbl.text = currentUser.user.userAddress;
    
    [self updateAmounts];
}

- (void)updateAmounts
{
    CurrentUser *currentUser = [CurrentUser getCurrentUser];
    
    self.subTotal.text = [NSString stringWithFormat:@"$ %0.2f", [currentUser.currentReceipt getSubTotalAmount]];
    self.taxTotal.text = [NSString stringWithFormat:@"$ %0.2f", [currentUser.currentReceipt getTaxAmount]];
    self.totalAmountLbl.text = [NSString stringWithFormat:@"$ %0.2f", [currentUser.currentReceipt getTotalAmount]];
    self.tipTextField.text = [NSString stringWithFormat:@"$ %0.2f", currentUser.currentReceipt.tip];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.receiptOrderTableView)
        return [[[[CurrentUser getCurrentUser] currentReceipt] orders] count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier;
    
    UITableViewCell *cell;
    
    if (indexPath.row != currentEditingIdx)
    {
        cellIdentifier = ReceiptOrderCellIdentifier;
        
        ReceiptOrderCell *receiptOrderCell = (ReceiptOrderCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (receiptOrderCell == nil) {
            receiptOrderCell = [[ReceiptOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        Order *order = [[[[CurrentUser getCurrentUser] currentReceipt] orders] objectAtIndex:indexPath.row];
        
        receiptOrderCell.receiptOrderName.text = order.menuName;
        receiptOrderCell.receiptOrderQuantity.text = [NSString stringWithFormat:@"%d x", order.quantity];
        receiptOrderCell.receiptOrderAmount.text = [NSString stringWithFormat:@"$ %0.2f", [order getOrderTotalPrice]];
        
        cell = receiptOrderCell;
    }
    else if (indexPath.row == currentEditingIdx)
    {
        cellIdentifier = ReceiptOrderEditCellIdentifier;
        
        ReceiptOrderEditCell *receiptOrderEditCell = (ReceiptOrderEditCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (receiptOrderEditCell == nil) {
            receiptOrderEditCell = [[ReceiptOrderEditCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        Order *order = [[[[CurrentUser getCurrentUser] currentReceipt] orders] objectAtIndex:indexPath.row];
        
        receiptOrderEditCell.receiptOrderName.text = order.menuName;
        receiptOrderEditCell.receiptOrderQuantity.text = [NSString stringWithFormat:@"%d x", order.quantity];
        
        receiptOrderEditCell.index = indexPath.row;
        receiptOrderEditCell.delegate = self;
        
        cell = receiptOrderEditCell;
    }
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */


#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != self.receiptOrderTableView)
        return;
    
    if (currentEditingIdx == indexPath.row) {
        currentEditingIdx = -1;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else {
        NSIndexPath *oldIndexPath = [NSIndexPath indexPathForRow:currentEditingIdx inSection:0];
        
        currentEditingIdx = indexPath.row;
        
        [tableView reloadRowsAtIndexPaths:@[indexPath, oldIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}


#pragma mark - ReceiptVCProtocol methods
- (void)incrementQuantity:(int)index {
    Receipt *receipt = [[CurrentUser getCurrentUser] currentReceipt];
    
    [receipt updateOrderAtIndex:index isIncrement:YES];
    
    [self.receiptOrderTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self updateAmounts];
}

- (void)decrementQuantity:(int)index {
    Receipt *receipt = [[CurrentUser getCurrentUser] currentReceipt];
    
    [receipt updateOrderAtIndex:index isIncrement:NO];
    
    [self.receiptOrderTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self updateAmounts];
}

@end
