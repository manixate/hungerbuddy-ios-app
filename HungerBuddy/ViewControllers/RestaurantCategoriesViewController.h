//
//  RestaurantCategoriesViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/13/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionHeaderView.h"

@interface RestaurantCategoriesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, SectionHeaderViewDelegate>
{
    NSArray *restaurantCategories;
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (nonatomic) NSMutableArray *sectionInfoArray;
@property (nonatomic) NSInteger openSectionIndex;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end
