//
//  ReceiptOrderEditCell.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/20/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "ReceiptOrderEditCell.h"
#import "ReceiptViewController.h"

@implementation ReceiptOrderEditCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.index = -1;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)plusBtnPressed:(id)sender {
    [self.delegate incrementQuantity:self.index];
}

- (IBAction)minusBtnPressed:(id)sender {
    [self.delegate decrementQuantity:self.index];
}
@end
