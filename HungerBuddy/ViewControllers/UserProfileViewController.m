//
//  UserProfileViewController.m
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "AFNetworking.h"
#import "AFNetworkActivityIndicatorManager.h"
#import "CurrentUser.h"
#import "UserProfileViewController.h"
#import "RecentOrderViewController.h"
#import "FollowersViewController.h"
#import "RestaurantCategoriesViewController.h"
#import "ActivityIndicator.h"
#import "UIImageView+AFNetworking.h"
#import "SocialViewController.h"
#import "DealsViewController.h"

@interface UserProfileViewController ()

@end

@implementation UserProfileViewController

@synthesize userNameLbl, userImageView, followingCountLbl, followersCountLbl, favoritesCountLbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidAppear:(BOOL)animated
{
    addressEditView = [[AddressEditView alloc] init];
    modalView = [[ModalView alloc] initWithSuperView:self.view andModalView:addressEditView];
    
    addressEditView.delegate = self;
    modalView.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self fillData];
    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kSettingsButton];
}

#pragma mark - Own Methods
- (void)fillData
{
    User *userInfo = [CurrentUser getUserInfo];
    
    if (userInfo != nil) {
        [userImageView setImageWithURL:userInfo.userImageURL];
        userNameLbl.text = [NSString stringWithFormat:@"Hello, %@", userInfo.userName];
        [self loadData:userInfo.userID];
        
        if (userInfo.userAddress && ![@"" isEqualToString:userInfo.userAddress])
        {
            self.addressLbl.text = userInfo.userAddress;
            self.addAddressBtn.enabled = false;
        }
        else
        {
            self.addressLbl.text = @"Add an address";
            self.addAddressBtn.enabled = true;
        }
    }
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

- (IBAction)searchBtnPressed:(id)sender {
}

- (IBAction)editAddressBtnPressed:(id)sender {
    [modalView showModalView];
}

- (IBAction)addAddressBtnPressed:(id)sender {
}

- (IBAction)followersBtnPressed:(id)sender {
    FollowersViewController *followersVC = [[FollowersViewController alloc] initWithNibName:@"FollowersViewController" bundle:nil];
    [self.navigationController pushViewController:followersVC animated:YES];
}

- (IBAction)recentOrdersBtnPressed:(id)sender {
    RecentOrderViewController *recentOrdersVC = [[RecentOrderViewController alloc] initWithNibName:@"RecentOrderViewController" bundle:nil];
    [self.navigationController pushViewController:recentOrdersVC animated:YES];
}

- (IBAction)foodBtnPressed:(id)sender {
    RestaurantCategoriesViewController *restaurantListVC = [[RestaurantCategoriesViewController alloc] initWithNibName:@"RestaurantCategoriesViewController" bundle:nil];
    [self.navigationController pushViewController:restaurantListVC animated:YES];
}

- (IBAction)socialBtnPressed:(id)sender {
    SocialViewController *socialVC = [[SocialViewController alloc] init];
    
    [self.navigationController pushViewController:socialVC animated:YES];
}

- (IBAction)dealsBtnPressed:(id)sender {
    DealsViewController *dealsVC = [[DealsViewController alloc] init];
    
    [self.navigationController pushViewController:dealsVC animated:YES];
}

- (IBAction)logoutBtnPressed:(id)sender {
    [CurrentUser clearCredentials];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)orderNowBtn:(id)sender {
    RestaurantCategoriesViewController *restaurantListVC = [[RestaurantCategoriesViewController alloc] initWithNibName:@"RestaurantCategoriesViewController" bundle:nil];
    [self.navigationController pushViewController:restaurantListVC animated:YES];
}

#pragma mark - AddressEditViewDelegate Methods
- (void)close {
    [modalView hideModalView];
}

- (void)saveWithAddressDictionary:(NSDictionary *)addressDictionary {
//    ActivityIndicator *activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    NSDictionary *parameters = @{@"name": name, @"address": address, @"city": city, @"postal_code": postalCode};
//    
//    [manager POST:EDIT_USER_INFO_URL parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Got response for updating user info: %@", responseObject);
//        NSLog(@"Updating user info successfull");
//        
//        User *currentUser = [CurrentUser getUserInfo];
//        currentUser.userCity = city;
//        currentUser.userAddress = address;
//        currentUser.userPostalCode = postalCode;
//        
//        [self fillData];
//
    [CurrentUser getCurrentUser].deliveryAddressDictionary = addressDictionary;
        [modalView hideModalView];
//
//        [activityIndicator stopAnimating];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Got error response for updating user info: %@", error);
//        NSLog(@"Updating user info failed");
//        
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Error changing user data." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
//        [alertView show];
//        
//        [modalView hideModalView];
//        
//        [activityIndicator stopAnimating];
//    }];
}

#pragma mark - ModalViewDelegate Methods
- (void)modalViewDidAppear:(ModalView *)modalView {

}

- (void)modalViewDidDisappear:(ModalView *)modalView {
    
}

#pragma mark - UITextFieldDelegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Own Methods
- (void)loadData:(int) userID {
    NSString *followerCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_FOLLOWER_COUNTER], userID];
    
    NSString *followingCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_FOLLOWING_COUNTER], userID];
    
    NSString *favoriteCounterURL = [NSString stringWithFormat: [NSString stringWithFormat:@"%@%@", URL_BASE, USER_LOVED_COUNTER], userID];
    
    NSLog(@"User Follow Counter URL: %@", followerCounterURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperation *followerCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:followerCounterURL]]];
    [followerCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            followersCountLbl.text = @"-1";
            
            return;
        }
        
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        followersCountLbl.text = [json objectForKey:@"count"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    NSLog(@"User Following Counter URL: %@", followingCounterURL);
    
    AFHTTPRequestOperation *followingCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:followingCounterURL]]];
    [followingCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            followingCountLbl.text = @"-1";
            
            return;
        }
        
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        followingCountLbl.text = [json objectForKey:@"count"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    AFHTTPRequestOperation *favoriteCounterRequest = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:favoriteCounterURL]]];
    
    [favoriteCounterRequest setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            favoritesCountLbl.text = @"-1";
            
            return;
        }
        
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        favoritesCountLbl.text = [json objectForKey:@"count"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ while processing request: %@", error, operation.request);
    }];
    
    NSArray *operations = [AFHTTPRequestOperation batchOfRequestOperations:@[followerCounterRequest, followingCounterRequest, favoriteCounterRequest] progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
    } completionBlock:^(NSArray *operations) {
        __block BOOL failed = false;
        [operations enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            AFHTTPRequestOperation *operation = obj;
            
            // We got error in one of our operations so show an alert.
            if (operation.error)
            {
                failed = true;
                *stop = true;
            }
        }];
        
        if (failed)
        {
            [Utility showAlert:@"Error getting complete user information"];
        }
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    }];
    
    [[NSOperationQueue mainQueue] addOperations:operations waitUntilFinished:NO];
}

- (void)loadUserImage:(NSString *)userID {
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithFrame:userImageView.frame];
    [userImageView addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://example.com/resources.json" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
