//
//  FinalReceiptViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 30/11/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConfirmedOrder;

@interface FinalReceiptViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    ConfirmedOrder *confirmedOrder;
}

- (id)initWithConfirmedOrder:(ConfirmedOrder *)aConfirmedOrder;

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *dayLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLbl;
@property (weak, nonatomic) IBOutlet UILabel *taxLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalItemLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;

@end
