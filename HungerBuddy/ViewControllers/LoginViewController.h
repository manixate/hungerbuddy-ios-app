//
//  LoginViewController.h
//  HungerBuddy
//
//  Created by sajjad ali on 9/28/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFHTTPRequestOperationManager.h"

@protocol LoginDelegate

@required

-(void)userLoggedIn;
-(void)userLoginCancelled;

@end

@interface LoginViewController : UIViewController
{
    id<LoginDelegate> delegate;
}

@property(nonatomic, retain) id<LoginDelegate> delegate;

@property(nonatomic, retain) IBOutlet UITextField *userTextField;
@property(nonatomic, retain) IBOutlet UITextField *passwordTextField;

- (IBAction)backBtnPressed:(id)sender;
- (IBAction)loginBtnPressed:(id)sender;
- (IBAction)facebookLoginBtnPressed:(id)sender;
- (IBAction)twitterLoginBtnPressed:(id)sender;

@end
