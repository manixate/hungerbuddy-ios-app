//
//  DealCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 16/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTCollectionView.h"

@interface DealCell : PSUICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIImageView *frontLayerImageView;
@end
