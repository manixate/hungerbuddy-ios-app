//
//  ConfirmOrderViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/21/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ActionSheetStringPicker.h>
#import <ActionSheetDatePicker.h>

@interface PaymentViewController : UIViewController {
    NSDate *date;
    
    ActionSheetStringPicker *dayPickerView;
    ActionSheetDatePicker *timePickerView;
    
    NSArray *days;
    
    NSInteger currentWeek, currentWeekday;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *userAddressLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalItemsLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLbl;
@property (weak, nonatomic) IBOutlet UIButton *dayBtn;
@property (weak, nonatomic) IBOutlet UIButton *timeBtn;

- (IBAction)dayBtnPressed:(id)sender;
- (IBAction)timeBtnPressed:(id)sender;
- (IBAction)creditBtnPressed:(id)sender;
- (IBAction)debitBtnPressed:(id)sender;
- (IBAction)cashBtnPressed:(id)sender;
@end
