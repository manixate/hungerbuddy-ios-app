//
//  OrderDetailCell.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 6/10/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *isVerifiedLbl;
@property (weak, nonatomic) IBOutlet UILabel *isProcessedLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@end
