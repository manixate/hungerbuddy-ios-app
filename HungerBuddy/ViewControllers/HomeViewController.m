//
//  LoginViewController.m
//  HungerBuddy
//
//  Created by sajjad ali on 9/28/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "HomeViewController.h"
#import "AFURLResponseSerialization.h"
#import "CurrentUser.h"
#import "UserProfileViewController.h"
#import "RecentOrderViewController.h"
#import "RestaurantCategoriesViewController.h"
#import "ActivityIndicator.h"
#import "SocialViewController.h"
#import "DealsViewController.h"
#import "SocialViewController.h"
#import "RestaurantSearchDetailViewController.h"
#import "ModalView.h"
#import "AddressEditView.h"
#import <AddressBookUI/AddressBookUI.h>
#import "AppDelegate.h"

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize zipTextField, dishTextField, loginButtonLbl, recentOrdersBtn;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    User *userInfo = [CurrentUser getUserInfo];
    if ([userInfo.userEmail isEqualToString:@""]) {
        loginButtonLbl.text = @"LOGIN";
        recentOrdersBtn.hidden = true;
    } else {
        loginButtonLbl.text = [NSString stringWithFormat:@"%@", userInfo.userName];
        recentOrdersBtn.hidden = false;
    }
    
    zipTextField.autocompleteDataSource = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

#pragma mark - LoginViewDelegate Methods
-(void)userLoggedIn
{
    NSLog(@"User Loged In");
//    User *userInfo = [CurrentUser getUserInfo];
//    loginButtonLbl.text = [NSString stringWithFormat:@"WELCOME BACK, %@", userInfo.userName];
    
//    [Utility showAlert:[NSString stringWithFormat:@"WELCOME BACK, %@", userInfo.userName]];
    [self.navigationController popViewControllerAnimated:TRUE];
}

-(void)userLoginCancelled
{
    [self.navigationController  popViewControllerAnimated:TRUE];
}

#pragma mark - HTAutocompleteTextFieldDataSource Methods
- (NSString *)textField:(HTAutocompleteTextField *)textField completionForPrefix:(NSString *)prefix ignoreCase:(BOOL)ignoreCase
{
    if (!prefix || prefix.length == 0)
        return @"";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"code BEGINSWITH[cd] %@", prefix];
    NSDictionary *location = [[appDelegate.locations filteredArrayUsingPredicate:predicate] firstObject];
    NSString *actualZipCode = location[@"code"];
    NSString *zipCode = [actualZipCode substringFromIndex:prefix.length];
    
    if (!zipCode)
        return @"";
    
    return zipCode;
}

#pragma mark - Actions

-(IBAction)showLogin:(id)sender
{
    User *userInfo = [CurrentUser getUserInfo];
    if ([userInfo.userEmail isEqualToString:@""]) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginView" bundle:nil];
        loginView.delegate = self;
        [self.navigationController pushViewController:loginView animated:true];
    } else {
        UserProfileViewController *profile = [[UserProfileViewController alloc] initWithNibName:@"UserProfileViewController" bundle:nil];
        [self.navigationController pushViewController:profile animated:true];
    }
    
}

-(IBAction)searchByZip:(id)sender {
    RestaurantSearchDetailViewController *restauranSearchDetailVC = [[RestaurantSearchDetailViewController alloc] initWithRestaurantAddress:zipTextField.text];
    
    [self.navigationController pushViewController:restauranSearchDetailVC animated:YES];
}

- (IBAction)searchByName:(id)sender {
    RestaurantSearchDetailViewController *restauranSearchDetailVC = [[RestaurantSearchDetailViewController alloc] initWithRestaurantName:dishTextField.text];
    
    [self.navigationController pushViewController:restauranSearchDetailVC animated:YES];
}

- (IBAction)recentOrdersBtnPressed:(id)sender {
    RecentOrderViewController *recentOrdersVC = [[RecentOrderViewController alloc] init];
    [self.navigationController pushViewController:recentOrdersVC animated:YES];
}

- (IBAction)foodBtnPressed:(id)sender {
    RestaurantCategoriesViewController *restaurantListVC = [[RestaurantCategoriesViewController alloc] init];
    [self.navigationController pushViewController:restaurantListVC animated:YES];
}

- (IBAction)socialBtnPressed:(id)sender {
    if (![CurrentUser isLoggedIn])
    {
        [Utility showAlert:@"Please log in first."];
        
        return;
    }
    SocialViewController *socialVC = [[SocialViewController alloc] init];
    
    [self.navigationController pushViewController:socialVC animated:YES];
}

- (IBAction)dealsBtnPressed:(id)sender {
    DealsViewController *dealsVC = [[DealsViewController alloc] init];
    
    [self.navigationController pushViewController:dealsVC animated:YES];
}

@end
