//
//  RestaurantSearchDetailViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 26/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantSearchDetailViewController : UIViewController {
    NSArray *restaurantArr;
    
    NSString *restaurantSearchURL;
}

- initWithRestaurantName:(NSString *)aRestaurantName;
- initWithRestaurantAddress:(NSString *)aRestaurantAddress;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
