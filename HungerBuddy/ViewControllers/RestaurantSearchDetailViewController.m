//
//  RestaurantSearchDetailViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 26/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "RestaurantSearchDetailViewController.h"
#import "Restaurant.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"
#import "CurrentUser.h"
#import "RestaurantDetailViewController.h"
#import "RestaurantCell.h"

static NSString *RestaurantCellIdentifier = @"RestaurantCellIdentifier";

@interface RestaurantSearchDetailViewController ()

@end

@implementation RestaurantSearchDetailViewController

- (id)init {
    self = [super initWithNibName:@"RestaurantSearchDetailViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        restaurantArr = @[];
    }
    return self;
}

- (id)initWithRestaurantName:(NSString *)aRestaurantName
{
    self = [self init];
    if (self) {
        restaurantSearchURL = [[NSString stringWithFormat:@"%@%@%@",URL_BASE,RS_SEARCH_DISH,aRestaurantName] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    return self;
}

- (id)initWithRestaurantAddress:(NSString *)aRestaurantAddress {
    self = [self init];
    if (self) {
        restaurantSearchURL = [[NSString stringWithFormat:@"%@%@%@",URL_BASE,RS_SEARCH_ZIP,aRestaurantAddress] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"RestaurantCell"
                                               bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:RestaurantCellIdentifier];
    
    self.tableView.rowHeight = RESTAURANT_ROW_HEIGHT;
    

    
    [self configureNavBarWithLeftButton:kBackButton titleButton:kContronPanelButton andRightButton:kConfirmButton];
    
    [self loadData];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

#pragma mark - Own Methods
#pragma mark - Load data
- (void)loadData {
    NSLog(@"Search URL: %@", restaurantSearchURL);
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager GET:restaurantSearchURL
      parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
         
         if (!responseObject)
         {
             NSLog(@"Got NULL response from: %@", operation.request);
             
             [indicator stopAnimating];
             [self.navigationItem enableNavigationItem];
             
             return;
         }
         
         NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
         
         NSError *error;
         restaurantArr = [MTLJSONAdapter modelsOfClass:Restaurant.class fromJSONArray:json error:&error];

         [self.tableView reloadData];

         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];

         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"error: %@", operation.responseString);
         NSLog(@"%ld", (long)operation.response.statusCode);
         [Utility showAlert:@"Could not fetch data"];
         
         [indicator stopAnimating];
         [self.navigationItem enableNavigationItem];
     }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return restaurantArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier:RestaurantCellIdentifier];
    if (cell == nil) {
        cell = [[RestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:RestaurantCellIdentifier];
    }
    
    Restaurant *restaurant = restaurantArr[indexPath.row];
    
    [cell.logoImageView setImageWithURL:restaurant.logoURL];
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greybar.PNG"]];
    cell.restaurantTitleLbl.text = restaurant.restaurantName;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    
    User *userInfo = [CurrentUser getUserInfo];
    if ([userInfo.userEmail isEqualToString:@""]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        return;
    }
    
    Restaurant *restaurant = restaurantArr[indexPath.row];
    RestaurantDetailViewController *detailViewController = [[RestaurantDetailViewController alloc] initWithNibName:@"RestaurantDetailViewController" bundle:nil andRestaurant:restaurant];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}

@end
