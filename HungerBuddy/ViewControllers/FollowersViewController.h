//
//  FollowersViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/8/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVSegmentedControl.h"
#import "SocialCell.h"

@interface FollowersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, SocialCellProtocol>

@property (nonatomic, strong) NSArray *socialList;

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *segmentControlView;
@property (strong, nonatomic) SVSegmentedControl *segmentControl;

@end
