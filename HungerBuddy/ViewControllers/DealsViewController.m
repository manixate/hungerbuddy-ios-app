//
//  DealsViewController.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 15/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import "DealsViewController.h"
#import "DealCell.h"
#import "DealsDetailViewController.h"
#import "ActivityIndicator.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Deal.h"

static NSString *cellIdentifier = @"DealCellIdentifier";

@interface DealsViewController () {
    NSArray *deals;
}

@end

@implementation DealsViewController

- (id)init
{
    self = [super initWithNibName:@"DealsViewController" bundle:[NSBundle mainBundle]];
    if (self) {
        deals = @[];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    PSUICollectionViewFlowLayout *flowLayout = [[PSUICollectionViewFlowLayout alloc] init];
    
    self.collectionView.collectionViewLayout = flowLayout;
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"DealCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:cellIdentifier];
    

    
    [self configureNavBarWithLeftButton:kHomeButton titleButton:kContronPanelButton andRightButton:kConfirmButton];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadData];
}

#pragma mark - Actions
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)rightBtnPressed:(id)sender {
}

#pragma mark - Own Methods
- (void)loadData{
    NSString *dealsURL = [NSString stringWithFormat:@"%@%@", URL_BASE, ALL_DEALS_URL];
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager GET:dealsURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            return;
        }
        
        NSArray *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        deals = [MTLJSONAdapter modelsOfClass:[Deal class] fromJSONArray:json error:&error];
        
        if (error) {
            NSLog(@"Error while deserializing deals model from json: %@", error);
        }
        
        [self.collectionView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
    }];
}

#pragma mark - PSUICollectionViewFlowLayoutDelegate Methods
- (CGSize)collectionView:(PSUICollectionView *)collectionView layout:(PSUICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(140, 140);
}

- (UIEdgeInsets)collectionView:(PSUICollectionView *)collectionView layout:(PSUICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

#pragma mark - PSUICollectionViewDataSource Methods
- (NSInteger)numberOfSectionsInCollectionView:(PSUICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(PSUICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return deals.count;
}

- (PSUICollectionViewCell *)collectionView:(PSUICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DealCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.backgroundView.backgroundColor = [UIColor blackColor];
    
    Deal *deal = deals[indexPath.row];
    
    if (deal.restaurantImageURL)
        [cell.imageView setImageWithURL: deal.restaurantImageURL];
    else
        cell.imageView.backgroundColor = [UIColor grayColor];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate Methods
- (void)collectionView:(PSUICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    DealsDetailViewController *dealsDetailVC = [[DealsDetailViewController alloc] initWithDeal:deals[indexPath.row]];
    
    [self.navigationController pushViewController:dealsDetailVC animated:YES];
}

@end
