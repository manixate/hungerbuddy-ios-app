//
//  OrderDetailViewController.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 6/9/14.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailViewController : UIViewController

- (id)initWithOrderID:(int)anOrderID;

@end
