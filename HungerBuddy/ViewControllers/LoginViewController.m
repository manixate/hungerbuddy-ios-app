//
//  LoginViewController.m
//  HungerBuddy
//
//  Created by sajjad ali on 9/28/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "AppDelegate.h"
#import "AFURLConnectionOperation.h"
#import "LoginViewController.h"
#import "CurrentUser.h"
#import "ActivityIndicator.h"
#import <CoreLocation/CoreLocation.h>
#import "AddressEditView.h"
#import "ModalView.h"
#import "AddressEditView.h"

static NSString *automaticOption = @"Automatic";
static NSString *defaultOption = @"Default";
static NSString *manualOption = @"Manual";

@interface LoginViewController () <ModalViewDelegate, AddressEditViewProtocol, UIAlertViewDelegate, CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    ActivityIndicator *activityIndicator;
    ModalView *modalView;
    
    UIAlertView *errorLocationAlertView;
    
    BOOL gotLocation;
}

@end

@implementation LoginViewController
@synthesize userTextField, passwordTextField;
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        
        geocoder = [[CLGeocoder alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //Assign Tags
    userTextField.tag = TAG_LOGIN_USERNAME;
    passwordTextField.tag = TAG_LOGIN_PASSWORD;
    
    errorLocationAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Cannot determine Location. Please enter it manually" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)backBtnPressed:(id)sender
{
    [self.delegate userLoginCancelled];
}

- (IBAction)loginBtnPressed:(id)sender
{
    NSString *email = self.userTextField.text;
    NSString *password = self.passwordTextField.text;
    
    if (!email.length || !password.length)
    {
        NSLog(@"Empty data. Email: %@, Password: %@", email, password);
        [Utility showAlert:@"Please enter complete information"];
        
        return;
    }
    
    [self performLoginOperation];
    [self hideKeyboard];
}

- (IBAction)facebookLoginBtnPressed:(id)sender {
    // get the app delegate so that we can access the session property
    //    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    // this button's job is to flip-flop the session from open to closed
    if (!FBSession.activeSession.isOpen){
        if (FBSession.activeSession == nil)
            FBSession.activeSession = [[FBSession alloc] init];
        if (FBSession.activeSession.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            FBSession.activeSession = [[FBSession alloc] init];
        }
        
        [FBSession.activeSession closeAndClearTokenInformation];
        [FBSession openActiveSessionWithReadPermissions:@[@"email"]
                                           allowLoginUI:YES
                                      completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                          [self fbLogin];
                                      }];
        
        // if the session isn't open, let's open it now and present the login UX to the user
        //        [appDelegate.fbSession openWithCompletionHandler:^(FBSession *session,
        //                                                         FBSessionState status,
        //                                                         NSError *error) {
        //            // and here we make sure to update our UX according to the new session state
        //            [self facebookLoggedIn:session sessionState:status error:error];
        //        }];
    }
}

- (IBAction)twitterLoginBtnPressed:(id)sender {
}

#pragma mark - Private Methods

- (void)hideKeyboard
{
	[userTextField resignFirstResponder];
	[passwordTextField resignFirstResponder];
}

-(void) performLoginOperation
{
//    NSURL *url = [NSURL URLWithString:URL_BASE];
    
    ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager POST:[NSString stringWithFormat:@"%@%@", URL_BASE, CMD_LOGIN]
       parameters:@{@"email": self.userTextField.text, @"password": self.passwordTextField.text}
          success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);

        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        if (!responseObject)
        {
            NSLog(@"Got NULL response from: %@", operation.request);
            
            [self userLoginFailed];
            
            return;
        }
        
        NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
        
        NSError *error;
        User *userInfo = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:json error:&error];
        if (!error)
        {
            [CurrentUser getCurrentUser].user = userInfo;
        }
        
        NSLog(@"response string: %@ ", operation.responseString);
        [self userLoggedIn];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", operation.responseString);
        NSLog(@"%ld", (long)operation.response.statusCode);
        
        [indicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        [self userLoginFailed];
    }];
}

- (void)fbLogin {
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (error) {
                 NSLog(@"FB Login Error: %@", error);
                 [self.delegate userLoggedIn];
                 return;
             }
             
             ActivityIndicator *indicator = [ActivityIndicator activityIndicatorForView:self.view];
             [self.navigationItem disableNavigationItem];
             
             AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
             manager.requestSerializer = [AFJSONRequestSerializer serializer];
             
             NSString *fbLoginURL = [NSString stringWithFormat:FB_LOGIN,
                                        user.objectID, [FBSession.activeSession accessTokenData].accessToken, [user objectForKey:@"email"],
                                        user.first_name, user.last_name, user.objectID];
             NSString *url = [NSString stringWithFormat:@"%@%@", URL_BASE, fbLoginURL];
             
             [manager GET:url
                parameters:nil
                   success:^(AFHTTPRequestOperation *operation, id responseObject)
              {
                  NSLog(@"JSON: %@", responseObject);
                  NSLog(@"operation hasAcceptableStatusCode: %ld", (long)operation.response.statusCode);
                  
                  
                  //sesl87aedrqvec7rdhlnuaisv3
                  [indicator stopAnimating];
                  [self.navigationItem enableNavigationItem];
                  
                  if (!responseObject)
                  {
                      NSLog(@"Got NULL response from: %@", operation.request.URL);
                      
                      [self userLoginFailed];
                      return;
                  }
                  
                  NSMutableDictionary *json = [NSJSONSerialization JSONObjectWithData:operation.responseData options:NSJSONReadingAllowFragments error:nil];
                  
                  NSError *error;
                  User *userInfo = [MTLJSONAdapter modelOfClass:User.class fromJSONDictionary:json error:&error];
                  if (!error)
                  {
                      [CurrentUser getCurrentUser].user = userInfo;
                  }
                  
                  NSLog(@"response string: %@ ", operation.responseString);
//                  [Utility showAlert:[NSString stringWithFormat:@"WELCOME BACK, %@", userInfo.userName]];
                  [self userLoggedIn];
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"error: %@", operation.responseString);
                  NSLog(@"%ld", (long)operation.response.statusCode);
                  
                  [indicator stopAnimating];
                  [self.navigationItem enableNavigationItem];
                  
                  [self userLoginFailed];
              }];
         }];
    }
}

- (void)userLoggedIn
{
    [self showLocationAlertView];
}

- (void)loginProcessCompleted
{
    [self.delegate userLoggedIn];
    [CurrentUser saveCurrentUser];
}

- (void)userLoginCancelled
{
    [self.delegate userLoginCancelled];
}

- (void)userLoginFailed
{
    [Utility showAlert:@"Invalid Email/Password"];
}

- (void)automaticLocation
{
    activityIndicator = [ActivityIndicator activityIndicatorForView:self.view];
    [self.navigationItem disableNavigationItem];
    [locationManager startUpdatingLocation];
    
    gotLocation = false;
    
    double delayInSeconds = 20.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if (gotLocation)
            return;
        
        [locationManager stopUpdatingLocation];
        [geocoder cancelGeocode];
        
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        activityIndicator = nil;
        
        if (!errorLocationAlertView.isVisible)
            [errorLocationAlertView show];
        
        gotLocation = true;
    });
}

- (void)defaultLocation
{
    if (![Utility isDefaultAddressPresent])
    {
        if (!errorLocationAlertView.isVisible)
            [errorLocationAlertView show];
    }
    else
    {
        NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *addressDictionary = [standardDefaults objectForKey:DEFAULT_ADDRESS];
        
        [CurrentUser getCurrentUser].deliveryAddressDictionary = addressDictionary;
        
        [Utility showAlert:[NSString stringWithFormat:@"Address is: %@", [CurrentUser getDeliveryAddressString]]];
        
        [self loginProcessCompleted];
    }
}

- (void)manualLocation
{
    // Get user location
    AddressEditView *addressView = [[AddressEditView alloc] init];
    modalView = [[ModalView alloc] initWithSuperView:self.view andModalView:addressView];
    
    addressView.delegate = self;
    modalView.delegate = self;
    
    [modalView showModalView];
}

- (void)showLocationAlertView
{
    if ([Utility isDefaultAddressPresent])
    {
        [[[UIAlertView alloc] initWithTitle:@"Location" message:@"Please select location method" delegate:self cancelButtonTitle:nil otherButtonTitles:automaticOption, defaultOption, manualOption, nil] show];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Location" message:@"Please select location method" delegate:self cancelButtonTitle:nil otherButtonTitles:automaticOption, manualOption, nil] show];
    }
}

#pragma mark - UIAlertViewDelegate Methods
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([@"Error" isEqualToString:alertView.title])
    {
        [self manualLocation];
    }
    else
    {
        NSString *optionTitle = [alertView buttonTitleAtIndex:buttonIndex];
        if ([optionTitle isEqualToString:automaticOption])
        {
            [self automaticLocation];
        }
        else if ([optionTitle isEqualToString:defaultOption])
        {
            [self defaultLocation];
        }
        else if ([optionTitle isEqualToString:manualOption])
        {
            [self manualLocation];
        }
    }
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *lastLocation = [locations lastObject];
    
    // if the location is older than 30s ignore
    if (fabs([lastLocation.timestamp timeIntervalSinceDate:[NSDate date]]) > 30)
        return;
    
    [CurrentUser setCurrentLocation:lastLocation];
    
    [manager stopUpdatingLocation];
    
    if (geocoder.isGeocoding)
        [geocoder cancelGeocode];
    
    [geocoder reverseGeocodeLocation:lastLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        CLPlacemark *placemark = [placemarks objectAtIndex:0];
        
        if (!placemark && !error)
        {
            // Means it was cancelled so don't show anything.
        }
        else if (error)
        {
            if (!errorLocationAlertView.isVisible)
                [errorLocationAlertView show];
            
            NSLog(@"Error occured while reverse geocoding: %@", error);
        }
        else
        {
            [CurrentUser getCurrentUser].deliveryAddressDictionary = placemark.addressDictionary;
            
            [Utility showAlert:[NSString stringWithFormat:@"Address is: %@", [CurrentUser getDeliveryAddressString]]];
            
            [self loginProcessCompleted];
        }
        
        [activityIndicator stopAnimating];
        [self.navigationItem enableNavigationItem];
        
        activityIndicator = nil;
        
        gotLocation = true;
    }];
}
#pragma mark - AddressEditViewDelegate Methods
- (void)close {
    [modalView hideModalView];
    
#warning Ask the client. Currently it returns to page.
    [self loginProcessCompleted];
}

- (void)saveWithAddressDictionary:(NSDictionary *)addressDictionary {
    [CurrentUser getCurrentUser].deliveryAddressDictionary = addressDictionary;
    [modalView hideModalView];
    [self loginProcessCompleted];
}

#pragma mark - ModalViewDelegate Methods
- (void)modalViewDidAppear:(ModalView *)modalView {
    
}

- (void)modalViewDidDisappear:(ModalView *)modalView {
    
}

@end
