//
//  RestaurantCell.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 26/01/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "RestaurantCell.h"

@implementation RestaurantCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
