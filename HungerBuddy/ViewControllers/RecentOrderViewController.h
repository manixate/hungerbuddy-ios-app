//
//  RecentOrderViewController.h
//  HungerBuddy
//
//  Created by Muhammad Azeem on 11/10/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecentOrderViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSArray *searchResults;
}

@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *orders;

@end
