//
//  LoginViewController.h
//  HungerBuddy
//
//  Created by sajjad ali on 9/28/13.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "HTAutocompleteTextField.h"

@interface HomeViewController : UIViewController<LoginDelegate, HTAutocompleteDataSource>
@property(nonatomic, retain) IBOutlet UILabel *loginButtonLbl;

@property(nonatomic, retain) IBOutlet HTAutocompleteTextField *zipTextField;
@property(nonatomic, retain) IBOutlet UITextField *dishTextField;
@property (weak, nonatomic) IBOutlet UIButton *recentOrdersBtn;
-(IBAction)showLogin:(id)sender;
-(IBAction)searchByZip:(id)sender;
- (IBAction)searchByName:(id)sender;
- (IBAction)recentOrdersBtnPressed:(id)sender;
- (IBAction)foodBtnPressed:(id)sender;
- (IBAction)socialBtnPressed:(id)sender;
- (IBAction)dealsBtnPressed:(id)sender;
@end
