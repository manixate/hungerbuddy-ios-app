//
//  AddressEditView.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 19/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddressEditViewProtocol <NSObject>

- (void)close;
- (void)saveWithAddressDictionary:(NSDictionary *)addressDictionary;

@end

@interface AddressEditView : UIView

@property (weak, nonatomic) id<AddressEditViewProtocol> delegate;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveAsDefaultCheckbox;

- (IBAction)closeBtnPressed:(id)sender;
- (IBAction)saveBtnPressed:(id)sender;
- (IBAction)saveAsDefaultBtnPressed:(id)sender;
@end
