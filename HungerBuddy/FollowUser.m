//
//  Follower.m
//  hungerbuddy
//
//  Created by Muhammad Azeem on 24/04/2014.
//  Copyright (c) 2014 Muhammad Azeem. All rights reserved.
//

#import "FollowUser.h"
#import "User.h"

@implementation FollowUser

#pragma mark - Object Mapping Methods
+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{
             @"followerID": @"follower_id",
             @"user": @"follower"
             };
}

+ (NSValueTransformer *)userJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:User.class];
}

@end