//
//  FoodOptionView.h
//  hungerbuddy
//
//  Created by Muhammad Azeem on 25/12/2013.
//  Copyright (c) 2013 Muhammad Azeem. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YIPopupTextView.h"

@class FoodItem;

@protocol FoodOptionDelegate <NSObject>

- (void)close:(FoodItem *)foodItem;

@end

@interface FoodOptionView : UIView <UITableViewDataSource, UITableViewDelegate, YIPopupTextViewDelegate> {
    FoodItem *foodItem;
    
    NSInteger currentSpice, currentSize, currentMeat;
    NSString *note;
    
    NSIndexPath *selectedIndexPath;
}

- (id)initWithFoodItem:(FoodItem *)aFoodItem;
- (void)setSelections;

@property (weak, nonatomic) id<FoodOptionDelegate> delegate;

@property (strong, nonatomic) UITableView *spicyTableView;
@property (strong, nonatomic) UITableView *sizeTableView;
@property (strong, nonatomic) UITableView *meatTableView;

@property (weak, nonatomic) IBOutlet UIView *spicyView;
@property (weak, nonatomic) IBOutlet UIView *sizeView;
@property (weak, nonatomic) IBOutlet UIView *meatView;
- (IBAction)confirmBtnPressed:(id)sender;
- (IBAction)closeBtnPressed:(id)sender;
- (IBAction)addNoteBtnPressed:(id)sender;
@end
